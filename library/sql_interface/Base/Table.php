<?php

//EXPECTED RETURNS
//
//array ('code' =>, 'description' =>, 'class' =>, 'output' => '');
//
//    CODE
//    > 1 - if successful db query
//    > 2 - inconsistent data inputed
//    > 3 - unknown db error
//
//
//CONSISTENCY CHECK
//> these features can be edited in the folling config file:
//* Configurations/db_configuration.php
//
//
//    FULL CHECK
//    -------------------------------------------------
//    > returns code 2 if:
//    > accessing columns that do not exist in the table
//    > associating incorect variable types with a column
//    > not identifing rows to update with a key
//    > trying to increment a non-integer column
//
//
//    DEFAULT_VALUES
//    ---------------------------------------------------
//    > if no values are given to column a default is given




require_once('Model.php');
require_once("Configurations/configurations.php");

class Table {
    const ADD = 1;
    const UPDATE = 2;
    const DELETE = 3;
    const GET = 4;
    
    protected $tableName;
    protected $model;
    
    public function __construct() {
        $this->model = new Model();
    }
          
    public function tableInfo() {
        GLOBAL $message;
        
                
        $query = "SHOW TABLE STATUS LIKE '$this->tableName'";
        $result = $this->model->query($query);
        if (!($result === false)) {
            $row = mysql_fetch_assoc($result);
            return array('code' => 1, 'message' => $message['db_success'], 'class' => get_class($this), 'output' => $row);
        } else {
            return array('code' => 3, 'message' => $message['db_unknown'], 'class' => get_class($this), 'output' => '');
        }
    }
     
    
    /*
     * Return the next auto increment as an integer
     */
    public function nextIncrement() {
        $result = $this->tableInfo();
        return $result['output']['Auto_increment'];
    }
    
    
     
     /*
      * 
      * $rows = array of $row
      * $row = array ('column_name' => value, ...)
      * 
      * COLUMNS MUST EXISTS IN DB
      * VALUE MUST BE OF CORRECT TYPE
      * FOR COLUMNS WITH NO SET VALUE A DEFAULT ONE WILL BE ASSIGNED
      * 
      */
   protected function addRows($rows) {  
   GLOBAL $message;
    
    $rows = $this->consistencyCheck($rows, Table::ADD);
    if ($rows === false) {
        return array ('code' => 2, 'message' => $message['db_inconsistent'], 'class' => get_class($this), 'output' => '');
    }
   
    $query = "INSERT INTO $this->tableName (";
    $commas = array (0,0,0);
    $keys = array();
    
    
    // add column names to query
    //
    foreach ($rows as $row) {
            $commas[0] = 0;
            foreach ($row as $key => $data) {
                                        

                //
                // if keys hasn't been added
                if (array_search($key, $keys) === false) {
                    $keys[] = $key;

                    if ($commas[0] == 1) {
                        $query .= ', ';
                        $commas[0] = 0;
                    }
                    $query .= $key;
                    $commas[0]++;

                }
            }
        }
    $query .= ") VALUES ";
    
    
    //
    // ADDING VALUES TO SQL STATEMENT
    foreach ($rows as $row) {

            if ($commas[1] == 1) {
                $query .= ', ';
                $commas[1] = 0;
            }
            $query .= "(";

            $commas[2] = 0;
            foreach ($keys as $key) {
                if ($commas[2] == 1) {
                    $query .= ', ';
                    $commas[2] = 0;
                }
                if (isset($row[$key])) {
                    $query .= "'" . $row[$key] . "'";
                } else {
                    $query .= "''";
                }
                $commas[2]++;
            }
            $query .= ")";
            $commas[1]++;
        }
    $query .= ';';
    
    
    $result = $this->model->query($query);
    if ($result) {
        return array('code' => 1, 'message' => $message['db_success'], 'class' => get_class($this), 'output' => '');
    } else {
        return array('code' => 3, 'message' => $message['db_unknown'], 'class' => get_class($this), 'output' => '');
    }
} 
    


/*
 *  $rows = array of row
 * 
 *  $row = array ('col' => value,...)
 * 
 * @ col = name of existing columns in db
 * @ value = encoded using the FUNCTION ENCODE(), use FUNCTION DECODE() to get data:
 *      > array ('value' =>, 'increment' =>)
 *      > value = the actual value
 *      > increment = whether to increment by that value or to set to that value
 * 
 * 
 *  $primary = name of column to use as primary key
 * 
 * COLUMNS MUST EXISTS IN DB
 * VALUE MUST BE OF CORRECT TYPE
 * A NON-INTEGER CANNOT BE INCREMENTED
 * A COLUMN IDENTIFING THE ROW WITH A PRIMARY KEY MUST BE AVAIABLE FOR EACH ROW
 * 
 */
protected function updateRows ($rows, $primary) {
    GLOBAL $message;
    
    $rows = $this->consistencyCheck($rows, Table::UPDATE, $primary);
    if ($rows === false) {
        return array ('code' => 2, 'message' => $message['db_inconsistent'], 'class' => get_class($this), 'output' => '');
    }

    $query = "UPDATE $this->tableName SET ";   
    $keys = array();
    $comma = 0;
    
    
  foreach ($rows as $row) {
      
      foreach ($row as $key => $data) {
          if ($key == $primary) {
              continue;
          }
          if (array_search($key, $keys) === false) {
              $keys[]= $key;
              
          } else {
              continue;
          }
          
          if ($comma == 1) {
              $query .= ', ';
              $comma = 0;
          }
          
          $query .= "$key = CASE $primary ";
          foreach ($rows as $row) {
              if (isset($row[$key])) {
                  
                  $decoded = $this->decode($data);
                  $decodedKey = $this->decode($row[$primary]);
                  
                  if ($decoded['increment']) {
                           $query .= "WHEN '" . $decodedKey['value']. "' THEN " . $key . " + " . $decoded['value'] . " ";
                        }
                   else {
                        $query .= "WHEN '" . $decodedKey['value'] . "' THEN '" . $decoded['value'] . "' ";
                  }
                  }
          }
          $query .= "ELSE $key END";
          $comma++;
          
      }
      
  }
  
  $result = $this->model->query($query);
  if ($result) {
        return array('code' => 1, 'message' => $message['db_success'], 'class' => get_class($this), 'output' => '');
    } else {
        return array('code' => 3, 'message' => $message['db_unknown'], 'class' => get_class($this), 'output' => '');
    }
}
 

/*
 * $rows = array of keys identifing the rows to delete
 * 
 *   
 * $primary = name of column to use as primary key
 * 
 * COLUMN BEING USED AS PRIMARY MUST EXISTS IN DB
 * KEYS MUST BE OF CORRECT TYPE
 * 
 */
protected function deleteRows ($rows, $primary) {
    GLOBAL $message;
    
    // creating array with correct format for consistencyCheck function
    $rows_assc = array();
    foreach ($rows as $id) {
        $rows_assc[] = array($primary => $id);
    }
    //
    
    $consistent = $this->consistencyCheck($rows_assc, Table::DELETE);
    if ($consistent === false) {
        return array ('code' => 2, 'message' => $message['db_inconsistent'], 'class' => get_class($this), 'output' => '');
    }
    
    $query = "DELETE FROM $this->tableName WHERE $primary IN (";
    $comma = 0;
    
    foreach ($rows as $row) {
        if ($comma == 1) {
            $query .= ", ";
            $comma = 0;
        }
        $query .= "'$row'";
        
        $comma++;
    }
    $query .= ");";
    
    $result = $this->model->query($query);
  if ($result) {
        return array('code' => 1, 'message' => $message['db_success'], 'class' => get_class($this), 'output' => '');
    } else {
        return array('code' => 3, 'message' => $message['db_unknown'], 'class' => get_class($this), 'output' => '');
    }
}


/*
 * $filters = array ('col' => value, ...)
 * 
 * name and values of columns to use as filters
 * 
 * COLUMNS MUST EXISTS ID DB
 * VALUE MUST BE OF CORRECT TYPE
 * 
 */
protected function getRowsBy ($filters) {
    GLOBAL $message;
    
    $consistent = $this->consistencyCheck(array($filters), Table::GET);
    if ($consistent === false) {
        return array ('code' => 2, 'message' => $message['db_inconsistent'], 'class' => get_class($this), 'output' => '');
    }
    $and = 0;
    
   $query = "SELECT * FROM $this->tableName";
   
   if (count($filters) > 0) {
       $query .= " WHERE ";
   }
   
   foreach ($filters as $key => $data) {
       if ($and == 1) {
           $query .= ' AND ';
           $and = 0;
       }
       
       $query .= "$key = '$data'";
       
       $and++;
   }
   $query .= ";";
   
   $result = $this->model->query($query);
   if ($result) {
       
       $table = array();
       while(true){
           $row = mysql_fetch_assoc($result);
           if ($row === false) {
               break;
           }
           $table[] = $row;
       }
       
       return array('code' => 1, 'message' => $message['db_success'], 'class' => get_class($this), 'output' => $table);
    } else {
        return array('code' => 3, 'message' => $message['db_unknown'], 'class' => get_class($this), 'output' => '');
    }
}



/*
 * $rows = array ('col_name' => value, ...)
 * 
 * $operation_type = [ADD, DELETE, UPDATE, GET]
 * 
 * $primary = name of column to use as primary key [used in when UPDATING]
 * 
 * $tableConfig = configuration for columns to which statement being checked relate
 * If none are given, an attempt will be made to retrieve the config automatically from db_configurations.php
 * 
 */
private function consistencyCheck ($rows, $operation_type, $primary = null, $tableConfig = null) {
    GLOBAL $db_table_config;
    GLOBAL $db_check_config;
    if (!isset($tableConfig)){
    $table = $db_table_config[$this->tableName];
    } else {
     $table = $tableConfig;
    }


    
    // IF FULL CHECK IS ENABLED 
    //
    if ($db_check_config['full_check']) {


            // IF PRIMARY IS SET, CHECK IF COLUMN EXISTS
            if (isset($primary)) {
                $res = $this->columnExists($primary, $table);

                // if primary column does not exist
                if (!$res) {
                    return false;
                }
            }
            

            // CHECK IF COLUMN EXISTS, IF TYPES ARE CONSISTENT AND IF NON-INTEGERS ARE BEING INCREMENTED
            //
                    $columns_checked = array();
            foreach ($rows as $row) {
                
                // check to see if primary key given for every row of rows
                // in case the OPERATION IS UPDATE
                if ($operation_type == Table::UPDATE) {
                    $all_row_keys = array_keys($row);
                    if (array_search($primary, $all_row_keys) === false) {
                        return false;
                    }
                }

                foreach ($row as $col_name => $data) {
                    if (!(array_search($col_name, $columns_checked) === false)) {
                        continue;
                    }
                    $columns_checked[] = $col_name;
                    $exists = $this->columnExists($col_name, $table);
                    // return false if invalid column found
                    if (!$exists) {
                        return false;
                    }
 

                    // DOES NOT ALLOW INCREMENTING NON-INTENGERS
                    if (($operation_type == Table::UPDATE)) {
                        $decoded = $this->decode($data);
                        
                        if (($decoded['increment']) && (gettype($decoded['value']) != 'integer')) {
                            return false;
                        }
                    }


                    // RETURN FALSE IF TYPE IS INCONSISTENT
                    if ($operation_type == Table::UPDATE) { 
                        $decoded = $this->decode($data); 
                        $type_check = $this->typeCheck($col_name, $decoded['value'], $table);
                    } else {
                        $type_check = $this->typeCheck($col_name, $data, $table);
                    }
                    if (!$type_check) {
                        return false; 
                    } 
                }
            }
        }
        //
        // IF FULL CHECK IS ENABLED
        
        
    //    
    // IF DEFAULT VALUE FEATURE IS ENABLED
    if (($db_check_config['default_values'])&&($operation_type == Table::ADD)) {

            foreach ($table as $col_name => $col_info) {

                foreach ($rows as $row_key => $row) {

                    $keys = array_keys($row);
                    if (array_search($col_name, $keys) === false) {
                        $rows[$row_key][$col_name] = $col_info['default_value'];
                    }
                }
            }
        }
    // IF DEFAULT VALUE FEATURE IS ENABLED
    //
        
   return $rows;
}


/*
 * $column = name of column to look up on the current table
 * $tableConfig = configuration array for each column of table being used
 * 
 */
private function columnExists($column, $tableConfig) {
    $table = $tableConfig;
    $exists = false;
    
    foreach ($table as $col_name => $col_info) {
        if ($column == $col_name){
            $exists = true;
            break;
        }
    }
    return $exists;
}


/*
 * checks if data if of right type for the column
 * 
 * $col_name = name of column
 * $data = value to be saved in the column
 * $tableConfig = configuration array for each column of table being used
 * 
 */
private function typeCheck($col_name, $data, $tableConfig) { 
    $table = $tableConfig;
    
    $type = $table[$col_name]['type'];
    $given_type = gettype($data);
    
    if ($type == 'timestamp') { 
        if ($this->isTimestamp($data)) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    
    if ($type == $given_type){
        return true; 
    } else {

        //IF EXPECTED TYPE IS INT AND STRING IS GIVEN
        //ATTEMPT TO CONVERT
        if (($type = 'integer')&&($given_type = 'string')) {
            $result = $this->convertableToInt($data);
            if ($result === false) {
                return false;
            } else {
                return true;
            }
        } else {
                return false;
        }
    }
}


/*
 * check to see if $data is formatted like :
 * 
 * @ timestamp <===>  YYYY-MM-DD HH:MM:SS
 * 
 * return true or false
 * 
 */
private function isTimestamp($data) {
    $fail = false;
    $data = str_split($data);
    
    if (count($data) != 19) {
        return false;
    }
    
    for ($i = 0; $i <= 18; $i ++) {
                
                if ($i == 4) {$i = 5;}
                if ($i == 7) {$i = 8;}
                if ($i == 10) {$i = 11;}
                if ($i == 13) {$i = 14;}
                if ($i == 16) {$i = 17;}
        if (
                ($data[$i] != '0')&&($data[$i] != '1')&&
                ($data[$i] != '2')&&($data[$i] != '3')&&
                ($data[$i] != '4')&&($data[$i] != '5')&&
                ($data[$i] != '6')&&($data[$i] != '7')&&
                ($data[$i] != '8')&&($data[$i] != '9')
            ){
            $fail = true;
            break;
        }
    }
    
    if (($data[4] != '-')||($data[7] != '-')){
        $fail = true;
    }
    if (($data[13] != ':')||($data[16] != ':')){
        $fail = true;
    }
    
    
    return (!$fail);
}



/*
 * converts a JSON string into an associative array
 * 
 */
private function decode($enc) {
    $dec = json_decode($enc, true);
    $output = array ('value' => $dec['value'], 'increment' => $dec['increment']);
    return $output;
}


/*
 * receives: $value = value to interpret
 * 
 * returns: JSON STRING of an array ('value' => value, 'increment' => true/false)
 * 
 * value = the actual value
 * increment = whether or not a + is present in the begging of the string
 * 
 * obs: function will attempt to convert value into an integer if a + is present 
 * 
 */
protected function interpretValue ($value) {
   if (gettype($value) != 'string') {
       return json_encode(array('value' => $value, 'increment' => false));
   } 
   
   //IS A STRING
   $str_array = str_split($value);
   
   
   //DOES NOT HAVE INCREMENT
   if ($str_array[0] != '+') {
       return json_encode(array('value' => $value, 'increment' => false));
   } 
   //HAS INCREMENT
   else {
       
       $new_value = str_replace('+','',$value);
       if ($this->convertableToInt($new_value)) {
           return json_encode(array('value' => (int)$new_value, 'increment' => true));
       } else {
           return json_encode(array('value' => $new_value, 'increment' => true));
       }
   }
}


/*
 * attempts to convert a string into a integer
 * returning the integer if SUCCESSFUL or 
 * FALSE otherwise
 * 
 */
private function convertableToInt($data) {
        
        
        // if input type is not a string return false
        if (gettype($data) == 'integer'){
            return true;
        } elseif (gettype($data) != 'string'){
            return false;
        }
    
        $fail = false;
        $array = str_split($data);

        foreach ($array as $char) {
            if (
                    ($char != '0') && ($char != '1') &&
                    ($char != '2') && ($char != '3') &&
                    ($char != '4') && ($char != '5') &&
                    ($char != '6') && ($char != '7') &&
                    ($char != '8') && ($char != '9')) {
                $fail = true;
                break;
                
            }
        }
            if (!$fail) {
                return true;
            } else {
                return false;
            }
        
    }

    /*
     * dtTablesColumns must not columns that appear more than once
     * in a table 
     * 
     * 
     * Generats a dtTableConfig array based on the given
     * dtTablesColumns array
     * 
     * dtTableConfig = array(columnName => columnConfig,...)
     * 
     */
    private function generateColumnsConfig($dtTablesColumns) {
        GLOBAL $db_table_config;
        $output = array();
        
       
            foreach ($dtTablesColumns as $key => $table) {
                
                if ((!is_array($table))&&($table == '*')){
                    
                    foreach($db_table_config[$key] as $column_name => $column_info){
                        $output[$column_name] = $column_info;
                    }
                    
                } else {
                    
                foreach ($table as $column) {
                    
                    $output[$column] = $db_table_config[$key][$column];
                    
                }
                    
                }
              
        }
        
        return $output;
        
        
    }
    
    
    /*
     *  Subquery has to have one of the following formats:
     *   (SELECT table1.col1-1, table2.col2-1... FROM image_tags, images WHERE ...) dt
     *   (SELECT table1.col1-1, table2.col2-1... FROM image_tags LEFT JOIN images ON ...) dt
     *   (SELECT * FROM table1, table2 WHERE ...) dt
     *   (SELECT * FROM table1 LEFT JOIN table2 ON ...) dt 
     *   
     *  Subquery must not generate a DERIVED TABLE where two or more column have the same name
     *  
     *  -----------------------------------------------------------------------------------------------
     *  -----------------------------------------------------------------------------------------------
     * 
     *  Analyzes the a SQL STATEMENT and generates a dtTablesColumns array.
     *  With the following structure:
     * 
     *  array = (tableName => array(columnName, columnName2, .... ))
     * 
     * 
     * @ tableName = name of real table that is part of derived table
     * @ columnNames = names of columns within the table that are related to the derived table
     * @ obs:. if all the columns of a table are part of the DT then instead of an array of columnName
     * @ the key will point to *;
     * 
     * @ Ex:. array (tableName => *, tableName2 => *)
     * @ Ex:. array (tableName => array(col1, col2), tableName2 => array( col3, col9))
     * 
     */
    private function interpretSubquery($subquery) {
        $output = array();
        $new_str = str_replace('(', '', $subquery);
        $new_str = str_replace(')', '', $new_str);
        $new_str = str_replace('SELECT', '', $new_str);
        $new_str = str_replace(',', '', $new_str);

        $array_str = explode(' ', $new_str);
        
        //removing unecessary empty spaces from array
        foreach ($array_str as $key => $word) {
            $array_str[$key] = str_replace(' ', '', $word);
            if (empty($word)){
                unset($array_str[$key]);
            }
        }
        
        //if * not present
        if (strpos($new_str,'*') === false) {
            $found = false;
            
            
            //remove all words ahead of and including FROM
            foreach ($array_str as $key => $word) {
            if ($found) {
                unset($array_str[$key]);
            }    
            if ((!$found)&&($word == 'FROM')) {
                unset($array_str[$key]);
                $found = true;
            }
            }
            
            //spliting each compound string [table_name.column_name]
            foreach ($array_str as $key => $word){
                $array_str[$key] = explode('.', $word);
            }
            
            //insert data into output array
            foreach ($array_str as $key => $compound) {
            if (array_key_exists($compound[0],$output) === false) {
                $output[$compound[0]] = array();
                $output[$compound[0]][] = $compound[1];
            } else {
                $output[$compound[0]][] = $compound[1];
            }
                
            }
                
            return $output;
            
                } else {
        //if * present
            
                    
                    if (strpos($new_str,'WHERE') === false){
                    // USING LEFT JOIN CLAUSE
                        
                        //remove all words up to and including FROM
                        foreach ($array_str as $key => $word) {
                            $found = false;
                            
                            if(!$found){
                                unset($array_str[$key]);
                            }
                            
                            if ((!$found)&&($word == 'FROM')){
                                $found = true;
                                unset($array_str[$key]);
                                break;
                            }
                        }
                        
                        
                        //remove all words ahead of and including ON
                        $found = false;
                        foreach ($array_str as $key => $word) {
                        if ($found) {
                        unset($array_str[$key]);
                        }    
                        if ((!$found)&&($word == 'ON')) {
                        unset($array_str[$key]);
                        $found = true;
                        }
                        }
                        
                        
                        //remove works JOIN and LEFT
                        $count = 0;
                        foreach($array_str as $key => $word){
                            if ($count == 2){
                                break;
                            }
                            if (($word == 'JOIN')||($word == 'LEFT')){
                                unset($array_str[$key]);
                                $count++;
                            }
                        }
                        
                        //inserting data into output array
                        foreach ($array_str as $key => $word){
                            $output[$word] = '*';
                        }
                        
                        return $output;
                    } else {
                    // USING WHERE CLAUSE
                        
                        
                        //remove all words up to and including FROM
                        $found = false;
                        foreach ($array_str as $key => $word) {
                            
                            if ((!$found)&&($word == 'FROM')) {
                                unset($array_str[$key]);
                                $found = true;
                                break;
                            }
                            
                            if(!$found){
                                unset($array_str[$key]);
                            }
                        }
                        
                        
                        //remove all words ahead of and including WHERE
                        $found = false;
                        foreach ($array_str as $key => $word){
                            
                            if ((!$found)&&($word == 'WHERE')){
                                $found = true;
                            }
                            
                            if($found) {
                                unset($array_str[$key]);
                            }
                        }
                        
                        
                        //inserting data into output array
                        foreach ($array_str as $key => $word){
                            $output[$word] = '*';
                        }
                        
                        
                        return $output;
                        
                    }                   
                    
                    
        }
    }
    
    
    
/*
 * $filters = array ('col' => value, ...)
 * name and values of columns to use as filters
 * 
 * $Subquery has to have one of the following formats:
 * 
 *   > (SELECT table1.col1-1, table2.col2-1... FROM image_tags, images WHERE ...) dt
 *   > (SELECT table1.col1-1, table2.col2-1... FROM image_tags LEFT JOIN images ON ...) dt
 *   > (SELECT * FROM table1, table2 WHERE ...) dt
 *   > (SELECT * FROM table1 LEFT JOIN table2 ON ...) dt 
 *   
 *   Subquery must not generate a DERIVED TABLE where two or more column have the same name
 * 
 * 
 * COLUMNS MUST EXISTS ID DB
 * VALUE MUST BE OF CORRECT TYPE
 * 
 */
    protected function getRowsFromDerivedTableBy ($subquery, $filters) {
        GLOBAL $message;
        
        $dtTablesColumns = $this->interpretSubquery($subquery);
        $tableConfig = $this->generateColumnsConfig($dtTablesColumns);
        
        $consistent = $this->consistencyCheck(array($filters), Table::GET, null, $tableConfig);
        if ($consistent === false) {
        return array ('code' => 2, 'message' => $message['db_inconsistent'], 'class' => get_class($this), 'output' => '');
    }
    
    
    $and = 0;
    
    $query = "SELECT * FROM $subquery";
   
   if (count($filters) > 0) {
       $query .= " WHERE ";
   }
   
   foreach ($filters as $key => $data) {
       if ($and == 1) {
           $query .= ' AND ';
           $and = 0;
       }
       
       $query .= "$key = '$data'";
       
       $and++;
   }
   $query .= ";";
   
   $result = $this->model->query($query);
   if ($result) {
       
       $table = array();
       while(true){
           $row = mysql_fetch_assoc($result);
           if ($row === false) {
               break;
           }
           $table[] = $row;
       }
       
       return array('code' => 1, 'message' => $message['db_success'], 'class' => get_class($this), 'output' => $table);
    } else {
        return array('code' => 3, 'message' => $message['db_unknown'], 'class' => get_class($this), 'output' => '');
    }
    
    
    
    
    }
    
}
    
?>

