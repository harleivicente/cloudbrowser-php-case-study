<?php

include_once('Classes/Base/Table.php');

class UsersTable extends Table {
    
     public function __construct() {
     parent::__construct();
     $this->tableName = 'users';
    }
    
    
     
/* $users = array ($user, ...)
 * 
 * $tag  =  array(
 *                  'user_id' =>,
 *                  'first_name' =>,
 *                  'last_name' => ,
 *                  'email' =>,
 *                  'password' =>,
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ FOR COLUMNS WITH NO SET VALUE A DEFAULT ONE WILL BE ASSIGNED
 * 
 */
 public function addUsers($users) { 
 return $this->addRows($users);
}




/* $users = array ($user, ...)
 * 
 * $tag  =  array(
 *                  'user_id' =>,
 *                  'first_name' =>,
 *                  'last_name' => ,
 *                  'email' =>,
 *                  'password' =>,
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ CANNOT INCREMENT NON-INTEGER COLUMNS
 * @ A PRIMARY KEY COLUMN MUST BE AVAIABLE FOR EACH ROW
 * 
 * @ OBS:. 'user_id' WILL BE USED AS THE PRIMARY KEY COLUMN
 *  
 * -----------------------------------------------------------------
 * -----------------   INCREMENTING VALUES   -----------------------
 * 
 * @ TO INCREMENT AN INTEGER WRITE IT AS A STRING AS PRECEDE IT WITH
 * @ + SIGN. EXAMPLE:
 * @
 * @ 'column' => '+1232'
 * @
 * @ OBS:. ONLY INTEGERS CAN BE INCREMENTED
 * 
 */
 public function updateUsers($users) {
     foreach ($users as $key => $user) {
         foreach ($user as $column => $data) {
             $users[$key][$column] = $this->interpretValue($data);
         }
     }

return $this->updateRows($users, 'user_id');
}



/*
 * Receives:
 * @ $users = array of primary key values
 * @ $primary = name of column to use as the PRIMARY KEY COLUMN
 *
 *                 POSSIBLE VALUES  
 *                  'user_id'
 *                  'first_name'
 *                  'last_name'
 *                  'email'
 *                  'password'
 *              
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function deleteUsers($users, $primary) {
  return $this->deleteRows($users, $primary);
}

/* $user  =  array(
 *                  'user_id' =>,
 *                  'first_name' =>,
 *                  'last_name' => ,
 *                  'email' =>,
 *                  'password' =>,
 *                  );
 * 
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function getUsersBy ($user) {
    return $this->getRowsBy($user);
}

    
}

?>