<?php

include_once('Classes/Base/Table.php');

class CollectionsTable extends Table {
    
     public function __construct() {
     parent::__construct();
     $this->tableName = 'collections';
    }
        
    
     
/* $collections = array ($collection, ...)
 * 
 * $collection  =  array(
 *                  'collection_id' =>,
 *                  'title' =>,
 *                  'short_description' => ,
 *                  'full_description' =>,
 *                  'owner_id' =>,
 *                  'cover_path' =>,
 *                  'category_id' =>,
 *                  'tagcloud_id' =>,
 *                  'access_type' =>,
 *                  'access_mode' =>,
 *                  'number_of_visits' =>,
 *                  'number_of_images' =>,
 *                  'date_added' =>,
 *                  'last_edit_on' =>,
 *                  'last_edit_by' =>,
 *                  'rating' =>
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ FOR COLUMNS WITH NO SET VALUE A DEFAULT ONE WILL BE ASSIGNED
 * 
 */
 public function addCollections($collections) { 
 return $this->addRows($collections);
}



/* $collections = array ($collection, ...)
 * 
 * $collection  =  array(
 *                  'collection_id' =>,
 *                  'title' =>,
 *                  'short_description' => ,
 *                  'full_description' =>,
 *                  'owner_id' =>,
 *                  'cover_path' =>,
 *                  'category_id' =>,
 *                  'tagcloud_id' =>,
 *                  'access_type' =>,
 *                  'access_mode' =>,
 *                  'number_of_visits' =>,
 *                  'number_of_images' =>,
 *                  'date_added' =>,
 *                  'last_edit_on' =>,
 *                  'last_edit_by' =>,
 *                  'rating' =>
 *                  );
 * 
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ CANNOT INCREMENT NON-INTEGER COLUMNS
 * @ A PRIMARY KEY COLUMN MUST BE AVAIABLE FOR EACH ROW
 * 
 * @ OBS:. 'collection_id' WILL BE USED AS THE PRIMARY KEY COLUMN
 *  
 * -----------------------------------------------------------------
 * -----------------   INCREMENTING VALUES   -----------------------
 * 
 * @ TO INCREMENT AN INTEGER WRITE IT AS A STRING AS PRECEDE IT WITH
 * @ + SIGN. EXAMPLE:
 * @
 * @ 'column' => '+1232'
 * @
 * @ OBS:. ONLY INTEGERS CAN BE INCREMENTED
 * 
 */
 public function updateCollections($collections) { 
     
     foreach ($collections as $key => $collection) {
         foreach ($collection as $column => $data) { 
             $collections[$key][$column] = $this->interpretValue($data);
         }
     }
     
 return $this->updateRows($collections, 'collection_id');
}


/*
 * Receives:
 * @ $collections = array of primary key values
 * @ $primary = name of column to use as the PRIMARY KEY COLUMN
 *
 *     > POSSIBLE PRIMARY COLUMNS    
 *       'collection_id'
 *       'title'
 *       'short_description'
 *       'full_description'
 *       'owner_id'
 *       'cover_path'
 *       'category_id'
 *       'tagcloud_id'
 *       'access_type'
 *       'access_mode'
 *       'number_of_visits'
 *       'number_of_images'
 *       'date_added'
 *       'last_edit_on'
 *       'last_edit_by'
 *       'rating'
 *                  
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function deleteCollections($collections, $primary) {
  return $this->deleteRows($collections, $primary);
}


/* 
 * $collection   =  array(
 *                  'collection_id' =>,
 *                  'title' =>,
 *                  'short_description' => ,
 *                  'full_description' =>,
 *                  'owner_id' =>,
 *                  'cover_path' =>,
 *                  'category_id' =>,
 *                  'tagcloud_id' =>,
 *                  'access_type' =>,
 *                  'access_mode' =>,
 *                  'number_of_visits' =>,
 *                  'number_of_images' =>,
 *                  'date_added' =>,
 *                  'last_edit_on' =>,
 *                  'last_edit_by' =>,
 *                  'rating' =>
 *                  );
 * 
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function getCollectionsBy ($collection) {
    return $this->getRowsBy($collection);
}

    
}

?>
