<?php

include_once('Classes/Base/Table.php');

class ImagesTable extends Table {
    
     public function __construct() {
     parent::__construct();
     $this->tableName = 'images';
    }
        
    
     
/* $images = array ($image, ...)
 * 
 * $image  =  array(
 *                  'image_id' =>,
 *                  'collection_id' =>,
 *                  'title' => ,
 *                  'description' =>,
 *                  'views' =>,
 *                  'average_rating' =>,
 *                  'date_created' =>,
 *                  'last_edit_on' =>,
 *                  'last_edit_by' =>,
 *                  'path' =>
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ FOR COLUMNS WITH NO SET VALUE A DEFAULT ONE WILL BE ASSIGNED
 * 
 */
 public function addImages($images) { 
 return $this->addRows($images);
}



/* $images = array ($image, ...)
 * 
 * $image  =  array(
 *                  'image_id' =>,
 *                  'collection_id' =>,
 *                  'title' => ,
 *                  'description' =>,
 *                  'views' =>,
 *                  'average_rating' =>,
 *                  'date_created' =>,
 *                  'last_edit_on' =>,
 *                  'last_edit_by' =>,
 *                  'path' =>
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ CANNOT INCREMENT NON-INTEGER COLUMNS
 * @ A PRIMARY KEY COLUMN MUST BE AVAIABLE FOR EACH ROW
 * 
 * @ OBS:. 'image_id' WILL BE USED AS THE PRIMARY KEY COLUMN
 *  
 * -----------------------------------------------------------------
 * -----------------   INCREMENTING VALUES   -----------------------
 * 
 * @ TO INCREMENT AN INTEGER WRITE IT AS A STRING AS PRECEDE IT WITH
 * @ + SIGN. EXAMPLE:
 * @
 * @ 'column' => '+1232'
 * @
 * @ OBS:. ONLY INTEGERS CAN BE INCREMENTED
 * 
 */
 public function updateImages($images) { 
     
     foreach ($images as $image) {
         foreach ($image as $column => $data) {
             $images[$image][$column] = $this->interpretValue($data);
         }
     }
     
 return $this->updateRows($images, 'image_id');
}






/*
 * Receives:
 * @ $images = array of primary key values
 * @ $primary = name of column to use as the PRIMARY KEY COLUMN
 *
 *     > POSSIBLE PRIMARY COLUMNS    
 *                  'image_id'
 *                  'collection_id'
 *                  'title'
 *                  'description'
 *                  'views'
 *                  'average_rating'
 *                  'date_created'
 *                  'last_edit_on'
 *                  'last_edit_by'
 *                  'path'
 *                 
 * 
 *                  
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function deleteImages($images, $primary) {
  return $this->deleteRows($images, $primary);
}


/* 
 * $image  =  array(
 *                  'image_id' =>,
 *                  'collection_id' =>,
 *                  'title' => ,
 *                  'description' =>,
 *                  'views' =>,
 *                  'average_rating' =>,
 *                  'date_created' =>,
 *                  'last_edit_on' =>,
 *                  'last_edit_by' =>,
 *                  'path' =>
 *                  );
 * 
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function getImagesBy ($image) {
    return $this->getRowsBy($image);
}



    
}

?>
