<?php

include_once('Classes/Base/Table.php');

class GroupsTable extends Table {
    
     public function __construct() {
     parent::__construct();
     $this->tableName = 'groups';
    }
        
    
     
/* $groups = array ($group, ...)
 * 
 * $group  =  array(
 *                  'group_id' =>,
 *                  'owner_id' =>,
 *                  'name' => 
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ FOR COLUMNS WITH NO SET VALUE A DEFAULT ONE WILL BE ASSIGNED
 * 
 */
 public function addGroups($groups) { 
 return $this->addRows($groups);
}



/* $groups = array ($group, ...)
 * 
 * $group  =  array(
 *                  'group_id' =>,
 *                  'owner_id' =>,
 *                  'name' => 
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ CANNOT INCREMENT NON-INTEGER COLUMNS
 * @ A PRIMARY KEY COLUMN MUST BE AVAIABLE FOR EACH ROW
 * 
 * @ OBS:. 'group_id' WILL BE USED AS THE PRIMARY KEY COLUMN
 *  
 * -----------------------------------------------------------------
 * -----------------   INCREMENTING VALUES   -----------------------
 * 
 * @ TO INCREMENT AN INTEGER WRITE IT AS A STRING AS PRECEDE IT WITH
 * @ + SIGN. EXAMPLE:
 * @
 * @ 'column' => '+1232'
 * @
 * @ OBS:. ONLY INTEGERS CAN BE INCREMENTED
 * 
 */
 public function updateGroups($groups) { 
     
     foreach ($groups as $key => $group) {
         foreach ($group as $column => $data) {
             $groups[$key][$column] = $this->interpretValue($data);
         }
     }
     
 return $this->updateRows($groups, 'group_id');
}






/*
 * Receives:
 * @ $groups = array of primary key values
 * @ $primary = name of column to use as the PRIMARY KEY COLUMN
 *
 *      POSSIBLE VALUES
 *                  'group_id'
 *                  'owner_id'
 *                  'name' 
 *                  
 *                  
 *              
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function deleteGroups($groups, $primary) {
  return $this->deleteRows($groups, $primary);
}


 /** $group  =  array(
 *                  'group_id' =>,
 *                  'owner_id' =>,
 *                  'name' => 
 *                  );
 * 
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function getGroupsBy ($group) {
    return $this->getRowsBy($group);
}

    
}

?>
