<?php

include_once('Classes/Base/Table.php');

class TagsTable extends Table {
    
     public function __construct() {
     parent::__construct();
     $this->tableName = 'tags';
    }
        
    
     
/* $tags = array ($collection, ...)
 * 
 * $tag  =  array(
 *                  'tag_id' =>,
 *                  'name' =>,
 *                  'description' => ,
 *                  'tagcloud_id' =>,
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ FOR COLUMNS WITH NO SET VALUE A DEFAULT ONE WILL BE ASSIGNED
 * 
 */
 public function addTags($tags) { 
 return $this->addRows($tags);
}



/* $tags = array ($collection, ...)
 * 
 * $tag  =  array(
 *                  'tag_id' =>,
 *                  'name' =>,
 *                  'description' => ,
 *                  'tagcloud_id' =>,
 *                  );
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * @ CANNOT INCREMENT NON-INTEGER COLUMNS
 * @ A PRIMARY KEY COLUMN MUST BE AVAIABLE FOR EACH ROW
 * 
 * @ OBS:. 'tag_id' WILL BE USED AS THE PRIMARY KEY COLUMN
 *  
 * -----------------------------------------------------------------
 * -----------------   INCREMENTING VALUES   -----------------------
 * 
 * @ TO INCREMENT AN INTEGER WRITE IT AS A STRING AS PRECEDE IT WITH
 * @ + SIGN. EXAMPLE:
 * @
 * @ 'column' => '+1232'
 * @
 * @ OBS:. ONLY INTEGERS CAN BE INCREMENTED
 * 
 */
 public function updateTags($tags) { 
     
     foreach ($tags as $key => $tag) {
         foreach ($tag as $column => $data) {
             $tags[$key][$column] = $this->interpretValue($data);
         }
     }
     
 return $this->updateRows($tags, 'tag_id');
}






/*
 * Receives:
 * @ $tags = array of primary key values
 * @ $primary = name of column to use as the PRIMARY KEY COLUMN
 *
 *                 POSSIBLE VALUES  
 *                  'tag_id'
 *                  'name'
 *                  'description'
 *                  'tagcloud_id'
 *                  
 *              
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function deleteTags($tags, $primary) {
  return $this->deleteRows($tags, $primary);
}


/* 
 * $tag  =  array(
 *                  'tag_id' =>,
 *                  'name' =>,
 *                  'description' => ,
 *                  'tagcloud_id' =>,
 *                  );
 * 
 * 
 * @ COLUMNS MUST EXISTS IN DB
 * @ VALUE MUST BE OF CORRECT TYPE
 * 
 */
public function getTagsBy ($tag) {
    return $this->getRowsBy($tag);
}

    
}

?>
