<?php

// Loading Abstract_class (library/Abstract_class.php) because Engine
// extends it.
require('library/Abstract_class.php');
class Engine extends Abstract_class{

    public function class_autoload($class){
        Utilities::register_system_message('Base Error.', $class . ' does not exist.');
    }

    /**
     * 
     * Start the system engine.
     * 
     * @access public
     */
    public function start(){
    // Sets up up autoload function "Engine::class_autoload()"
        spl_autoload_register(function ($class) {
            Engine::class_autoload($class);
        });
        
    // load most frequently used classes
        $this->load_classes();

    // start the enviroment variable. Set default values and define
    // array structure for variable.
        $this->start_enviroment_variable();

        Utilities::register_system_message('Fatal error', 'Message trigger from within engine execution.');
        Utilities::register_system_message('Engine booting error', 'Message trigger from within engine execution.');
    // Load data into the enviroment varible.
        $this->load_enviroment_variable();
        
    // route system
        $this->route_system();

    }

    /**
     * start_enviroment_variable
     * 
     * Setups up enviroment variable. Has the following format:
     * $config = array(
     *
     *      'system_outline' => array(
     *           'classes' => array ('class_name' => 'class_path', ...),
     *           'controllers' => array ('controller_name' => 'controller_path', ...),
     *           'url_handlers' => array ('url_handler_name' => 'url_handler_path', ...),
     *           'frames' => array('frame_name' => 'frame_path', ...),
     *           'pages' => array('page_name' => 'page_path', ...),
     *           'components' => array('component_name' => 'component_path', ...),
     *           'objects' => array('object_name' => 'object_path', ...),
     *           'js' => array('js_name' => 'js_path', ...),
     *           'css' => array('css_name' => 'css_path', ...)),
     *           
     *
     *      'language' => array('code' => 'clear_text', ...),
     *
     *      'system_config' => array('system_config_array'),
     * 
     *      'system_messages' => array('message_type' => array('msg_1', 'msg_2', ...), ....),
     * 
     *      'loaded_elements' => array('js' => array('js_name', ...), 'css' => array('css_name', ...))
     * );
     * 
     */
    private function start_enviroment_variable() {
        GLOBAL $config;

        $config = array(
            'system_outline' => array(),
            'language' => array(),
            'system_config' => array(),
            'messages' => array()
            );
    }

    /**
     * load_enviroment_variable
     * 
     */
    private function load_enviroment_variable() {
        GLOBAL $config;

        require('configuration/system/system_config.php');

        $config['system_outline'] = $this->get_system_outline();
        $config['language'] = array();
        $config['system_config'] = $system_config;
    
    }
    

    private function load_classes() {
        $load_files = array(
            'library/Abstract_class.php',
            'library/engine/Utilities.php',
            'library/control/Controller.php',
            'library/view/Viewer.php',
            );

        foreach($load_files as $load_file){
            require_once($load_file);
        }

    }


    

    private function route_system() {
        GLOBAL $config;

        $url = Utilities::current_page_url();
        $url = str_replace($config['system_config']['site_url'], "", $url);

        $parts = explode('?', $url);
        $url = explode('/', $parts[0]);

        // Sends control to appropriate url_handler
        $handler = $url[0];

        if(!isset($handler) || empty($handler)){
            $handler = 'standard';
        } 

        if(!isset($config['system_outline']['url_handlers'][$handler])){
            $handler = 'standard';
        }

        $path = Utilities::get_url_handler_path($handler);
        if(!file_exists($path . "/" . $handler . ".php")){
        throw new Exception("The url_handler file: $path/$handler.php, does not exist.");
        }

        require($path . "/" . $handler . ".php");
    }


    /**
     * get_system_outline
     *
     * @return array - returns the array describing the folder structure of the system.
     *                 as defined in "scan_system".
     * 
     */
    private function get_system_outline() {
        // in the future it will verify if this information is already avaiable in the cache.
        // If it is it'll load it from there. Otherwise it'll get the information from 'scan_system'
        // and then store in the cache (cache/).

        return $this->scan_system();
    }

    /**
     * scan_system
     * 
     * Scan systems folder and creates an array outlining
     * the structure of classes, controllers, frames, pages, components, objects, js and css files.
     * The returned array has the following format:
     * array = array(
     *          'classes' => array('class_name' => 'path', ... ),
     *          'controllers' => array('controller_name' => 'path', ... ),
     *          'frames' => array('frame_name' => 'path', ... ),
     *          'pages' => array('page_name' => 'path,' ... ),
     *          'components' => array('component_name' => 'path,' ... ),
     *          'objects' => array('object_name' => 'path,' ... ),
     *          'js' => array('js_name' => 'path,' ... ),
     *          'css' => array('css_name' => 'path,' ... ),
     * );
     *
     * @return array
     */
    private function scan_system() {
        return array(
            'classes' => $this->scan_classes(),
            'controllers' => $this->scan_controllers(),
            'url_handlers' => $this->scan_url_handlers(),
            'frames' => $this->scan_frames(),
            'pages' => $this->scan_pages(),
            'components' => $this->scan_components(),
            'objects' => $this->scan_objects(),
            'html' => $this->scan_html(),
            'js' => $this->scan_js(),
            'css' => $this->scan_css()
            );
    }

    /*
    *   Scans library folder and app/<app_name>/library
    */
    private function scan_classes() {
        return $this->scan_root_app('library');
    }


    /*
    *   Scans site/control and app/<app_name>/site/control
    */
    private function scan_controllers() {
        return $this->scan_root_app('site/control');
    }

    /*
    *   Scans site/url_handler and app/<app_name>/site/url_handler
    */
    private function scan_url_handlers() {
        return $this->scan_root_app('site/url_handler');
    }

    /*
    *   Scans site/view/frame and app/<app_name>/site/view/frame
    */
    private function scan_frames() {
        return $this->scan_root_app('site/view/frame');
    }

    /*
    *   Scans site/view/html and app/<app_name>/site/view/html
    */
    private function scan_html() {
        return $this->scan_root_app('site/view/html');
    }

    /*
    *   Scans site/view/page and app/<app_name>/site/view/page
    */
    private function scan_pages() {
        return $this->scan_root_app('site/view/page');
    }

    /*
    *   Scans site/view/component and app/<app_name>/site/view/component
    */
    private function scan_components() {
        return $this->scan_root_app('site/view/component');
    }

    /*
    *   Scans site/view/object and app/<app_name>/site/view/object
    */
    private function scan_objects() {
        return $this->scan_root_app('site/view/object');
    }

    /*
    *   Scans site/view/js and app/<app_name>/site/view/js
    */
    private function scan_js() {
        return $this->scan_root_app('site/view/js');
    }

    /*
    *   Scans site/view/css and app/<app_name>/site/view/css
    */
    private function scan_css() {
        return $this->scan_root_app('site/view/css');
    }

    /**
     * scan_root_app
     * 
     * Scans a $folder within the root of the system and within
     * app/<app_name>/ and generates an array in the format:
     *      array('file_name' => 'path', ...);
     * 
     * @param string $folder - Name of folder to scan.
     * @param array
     */
    private function scan_root_app($folder){
        $output = array();

        $app_folders = scandir('app');
        array_shift($app_folders);
        array_shift($app_folders);
        $pile = array($folder);

        if(is_array($app_folders)){
            foreach($app_folders as $app_folder){
                $pile[] = 'app/' . $app_folder . "/$folder";
            }
        }
        
        while(!empty($pile)){

            $first = $pile[0];
            if(is_dir($first)){

                // Removes '.' and '..' as entries.
                $items = scandir($first);
                array_shift($items);
                array_shift($items);
                
                foreach ($items as $key => $item){

                // item is a folder
                    if(is_dir($first . "/" . $item)){
                        array_push($pile, $first . "/" . $item);
                // item is a file
                    } else {
                        $item = explode(".", $item);
                        $output[$item[0]] = $first;
                    }
                }
                

            }
            array_shift($pile);
        }
        return $output;
    }


}

?>
