<?php

/**
* Utilities
*
* Class that contains multiple static methods that 
* can be used anywhere in the site.
*/
class Utilities extends Abstract_class {
     
    

    /**
     * get_config - get the value of a variable within
     * the $config (enviroment variable)
     * 
     * @param array $location - array that indicates the 
     * location of a variable in the $config associative array by
     * listing the indexes that lead to the variable.
     *
     * @return mixed - contents of index indicated in the $config
     * associative array OR FALSE if index not set.
     * 
     */
    public function get_config($location) {
        GLOBAL $config;

        $string = "";
        foreach($location as $location){
            $string .= "['" . $location . "']";
        }

        eval( "\$set = isset(\$config" . $string . ");");
        if($set){
            eval("\$value = \$config" . $string . ";");
            return $value;
        } else {
            return false;
        }
    }

    /**
     * set_config - sets the value of a an index in an associative array.
     * 
     * @param array $location - array of indexes that lead to the variable
     * in the $config array.
     * 
     * @param mixed $value
     *
     */
    public function set_config($location, $value) {
        GLOBAL $config;

        $string = "";
        foreach($location as $location){
            $string .= "['" . $location . "']";
        }
    
        eval ("\$config" . $string . " = \$value ;");
    }

    /**
     * extract_config
     * 
     * @param array $location - array of indexes that lead to the variable
     * in the $config array.
     *
     * @return mixed value.
     */
public function extract_config($location) {
    $return = Utilities::get_config($location);
    Utilities::set_config($location, null);
    return $return;
}
    /**
     * push_config - puts a value at the end on an array located in $config.
     * 
     * @param array $location - array of indexes that lead to the variable
     * in the $config associative array.
     * 
     * @param mixed $value
     *
     */
    public function push_config($location, $value) {
        $current = Utilities::get_config($location);

        // variable not set
        if($current === false){
            Utilities::set_config($location, array($value));
        
        // is array
        } elseif(is_array($current)) {
            $current[] = $value;
            Utilities::set_config($location, $current);

        // is set but is not an array
        } else {
            Utilities::set_config($location, array($current, $value));
        }
    }

    /**
     * get_site_url
     * 
     * @return string
     */
    public function get_site_url(){
        return Utilities::get_config(array('system_config', 'site_url'));
    }


    /**
     * register_system_message - Registers a message in $config within a specifig
     * category.
     * 
     * @param string $type - Type of message.
     * @param string $message
     *
     */
    public function register_system_message($type, $message) {

        Utilities::push_config(array('system_messages', $type), $message);
    }


    /**
     * extract_messages - Extracts current system messages from the enviroment
     * variable $config.
     * 
     * @param string|null $type - Type of messages to retrieve or NULL to extract all.
     *
     * @return array
     */
    public function extract_system_messages($type = null)
    {
        if($type !== null){
            $messages = Utilities::extract_config(array('system_messages', $type));
            if($messages === false){
                return array();
            } else {
                return $messages;
            }
        } else {
            $messages = Utilities::extract_config(array('system_messages'));
            if($messages === false){
                return array();
            } else {
                return $messages;
            }
        }
    }

    /**
     * get_frame_path - Gets the path of the frame file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_frame_path($frame_name)
        {
            return Utilities::get_config(array('system_outline', 'frames', $frame_name));
        } 

    /**
     * get_page_path - Gets the path of the page file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_page_path($page_name)
        {
            return Utilities::get_config(array('system_outline', 'pages', $page_name));
        } 

    /**
     * get_component_path - Gets the path of the component file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_component_path($component_name)
        {
            return Utilities::get_config(array('system_outline', 'components', $component_name));
        } 

    /**
     * get_object_path - Gets the path of the object file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_object_path($object_name)
        {
            return Utilities::get_config(array('system_outline', 'objects', $object_name));
        }    

    /**
     * get_html_path - Gets the path of the html element file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_html_path($html_name)
        {
            return Utilities::get_config(array('system_outline', 'html', $html_name));
        }


    /**
     * get_js_path - Gets the path of the js file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_js_path($js_name)
        {
            return Utilities::get_config(array('system_outline', 'js', $js_name));
        }

    /**
     * get_url_handler_path - Gets the path of the url_handler file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_url_handler_path($url_handler_name)
        {
            return Utilities::get_config(array('system_outline', 'url_handlers', $url_handler_name));
        }


    /**
     * get_css_path - Gets the path of the css file.
     * 
     * @return string|FALSE - Returns the path as a string or FALSE if
     * not found.
     */
    public function get_css_path($css_name)
        {
            return Utilities::get_config(array('system_outline', 'css', $css_name));
        } 



    public function current_page_url() {
        $pageURL = 'http';
        if ((isset($_SERVER['HTTPS']))&&($_SERVER["HTTPS"] == "on")) {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }

        return $pageURL;
        }


}


?>
