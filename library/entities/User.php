<?php

include_once('Classes/TableClasses/CollectionsTable.php');
include_once('Classes/TableClasses/ImagesTable.php');
include_once('Classes/TableClasses/TagsTable.php');
include_once('Classes/TableClasses/UsersTable.php');
include_once('Classes/TableClasses/GroupsTable.php');
include_once('Classes/TableClasses/CollectionAdminsTable.php');
include_once('Classes/TableClasses/EntriesTable.php');
include_once('Classes/Library.php');

// User Class
// 
//  Output
//  ------------------------------
//  code 1 -> success
//  code 2 -> invalid input
//  code 3 -> no permission
//  code 4 -> user not found
//  code 5 -> missing arguments
//  code 6 -> user already logged in
//  
//  code 7 -> username is incorrect
//  code 8 -> password is incorrect
//  
//  code 9 -> user object empty
//  code 10 -> fail
//
//  Fields
//  -------------------------------
//  @loggedIn - boolean - whether or not the user is logged in
//  @userInfo - array - ('user_id', 'first_name', 'last_name', 'email', 'username', 'password')
//
//  Constructor
//  -------------------------------
//  When an instance of User is created it will load the info of the logged in
//  user. IF no user is logged in the it will consider the user to be a GUEST
//
class User {

    //info
    private $userInfo;
    private $loggedIn;
    
    
    //table
    public $collectionsTable;
    public $imagesTable;
    public $tagsTable;
    public $usersTable;
    public $groupsTable;
    public $collectionAdminsTable;
    public $entriesTable;

    //cache    


    public function __construct($userId = null) {
        GLOBAL $message;
        $this->loggedIn = false;
        $this->userInfo = null;

        $this->collectionsTable = new CollectionsTable();
        $this->imagesTable = new ImagesTable();
        $this->tagsTable = new TagsTable();
        $this->usersTable = new UsersTable();
        $this->groupsTable = new GroupsTable();
        $this->collectionAdminsTable = new CollectionAdminsTable();
        $this->entriesTable = new EntriesTable();

        // UserId given
        //
        if (isset($userId)) {
            $users = $this->usersTable->getUsersBy(array('user_id' => $userId));
            if (count($users['output']) != 0) {
                $user = $users['output'][0];

                $this->userInfo = $user;

                //Checks if loaded user is logged in
                if (Session::get_variable('loggedIn')) {

                    if (Session::get_variable('userId') == $this->userInfo['user_id']) {
                        $this->loggedIn = true;
                    } else {
                        $this->loggedIn = false;
                    }
                }
            } else { 
                die(print_r(array('code' => 4, 'message' => $message['user_not_found'], 'class' => get_class($this), 'output' => '')));
            }
        } else {
            // UserId not given
            //
            
        $this->loadLoggedUser();
        }
    }

    /*
     * Creates a collection
     * 
     * $collection  =  array(
     *                      'title' =>,
     *                      'short_description' => ,
     *                      'full_description' =>,
     *                      'category_id' =>,
     *                      'access_type' =>,
     *                      'access_mode' =>,
     *                      );
     */

    public function createCollection($collection) {
        GLOBAL $message;

        // Check if user has permission to access it
        $hasPermission = $this->hasPermission('collection_create');
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }

        // Check if input is valid
        $valid = true; {
            $valid = true;
        }
        if (!$valid) {
            return array('code' => 2, 'message' => $message['user_invalid_input'], 'class' => get_class($this), 'output' => '');
        }


        // Next increment for collections table
        $next = $this->collectionsTable->nextIncrement();

        $params = array(
            'owner_id' => $this->userId,
            'cover_path' => $next . '_cover',
            'tagcloud_id' => $next, // use collection id as tagcloud id
            'number_of_visits' => 0,
            'number_of_images' => 0,
            'date_added' => Library::currentTimestamp(),
            'last_edit_on' => Library::currentTimestamp(),
            'last_edit_by' => $this->userId,
            'rating' => 0
        );

        if (isset($collection['title'])) {
            $params['title'] = $collection['title'];
        }
        if (isset($collection['short_description'])) {
            $params['short_description'] = $collection['short_description'];
        }
        if (isset($collection['full_description'])) {
            $params['full_description'] = $collection['full_description'];
        }
        if (isset($collection['category_id'])) {
            $params['category_id'] = $collection['category_id'];
        }
        if (isset($collection['access_type'])) {
            $params['access_type'] = $collection['access_type'];
        }
        if (isset($collection['access_mode'])) {
            $params['access_mode'] = $collection['access_mode'];
        }




        // Add collection
        $result = $this->collectionsTable->addCollections(array($params));
        if ($result['code'] != 1) {
            return $result;
        } else {
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }
    }
    
    
    /**
     * @param string $name name of new group
     * 
     */
    public function createGroup($name) {
        
        // Check if user has permission to access it
        $hasPermission = $this->hasPermission('user_edit', array('admin' => $this->userInfo['admin']));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
     
        
        // Check if input is valid
        {
            $valid = true;
        }
        if (!$valid){
            return array('code' => 2, 'message' => $message['user_invalid_input'], 'class' => get_class($this), 'output' => '');
        }
        
        
        // Create Group
        $result = $this->groupsTable->addGroups(array($name));
        if ($result['output'] != 1){
            return $result;
        } else {
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }
        
    }
    
    
     /*
     *  $groupId - id of group to delete
     * 
     */
    public function deleteGroup($groupId) {
        GLOBAL $message;

        // Check if user has permission to delete collection
        $hasPermission = $this->hasPermission('user_edit',array('admin' => $this->userInfo['admin']));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }


        // Check if group exists
        $fail = true;

        $group = $this->groupsTable->getGroupsBy(array('group_id' => $groupId));
        if (count($group['output']) != 0) {
            $fail = false;
        }

        if ($fail) {
            return array('code' => 10, 'message' => $message['group_not_found'], 'class' => get_class($this), 'output' => '');
        }


        // Delete group
        $result = $this->groupsTable->deleteGroups(array($groupId), 'group_id');
        if ($result['code'] != 1) {
            return $result;
        } else {
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }


        // Develop MySql Query to remove:
        // delete group members from GROUP MEMBERS TABLE
    }
    
    

    /*
     *  $collectionId - id of collection to delete
     * 
     *  Removes collectioins by id
     * 
     *  The system will also remove the tagcloud, the tags and the images related to this collection
     */

    public function deleteCollection($collectionId) {
        GLOBAL $message;

        // Check if user has permission to delete collection
        $hasPermission = $this->hasPermission('collection_delete', array('collection_id' => $collectionId));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }


        // Check if collection exists
        $fail = true;

        $col = $this->collectionsTable->getCollectionsBy(array('collection_id' => $collectionId));
        if (count($col['output']) != 0) {
            $fail = false;
        }

        if ($fail) {
            return array('code' => 4, 'message' => $message['collection_not_found'], 'class' => get_class($this), 'output' => '');
        }


        // Delete collection
        $result = $this->collectionsTable->deleteCollections(array($collectionId), 'collection_id');
        if ($result['code'] != 1) {
            return $result;
        } else {
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }


        // Develop MySql Query to remove:
        // delete images from images table
        // delete tags from tags table
        // delete collections admins from collection admins table
        // delele collections guests from collection guests table
        // delete entries for collections admins/guests from entries table
        // delete comments for images of collection from images table
    }

    
    /*
     * Creates a REGUALR USER or SITE ADMINISTRATOR
     * 
     * $user = array(
     *              'first_name' =>,
     *              'last_name' =>,
     *              'email' =>,
     *              'username' =>,
     *              'password' =>,  -  real string password 
     *              );
     * @ All these values are required
     * 
     * $admin = boolean --- if user is a regular user or site
     * admin
     * 
     */
    public function createUser($user, $admin = false) {
        GLOBAL $message;

        // Check if user has permission
        $hasPermission = $this->hasPermission('user_creation', array('admin' => $admin));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }

        // Check if input is valid 
        // Check is email / username are not already taken
        {
            $valid = true;
        }
        if (!$valid) {
            return array('code' => 2, 'message' => $message['user_invalid_input'], 'class' => get_class($this), 'output' => '');
        }

        
        // Check if any arguments are missing
        // 
        $fail = false;
        $params = array();
        if (isset($user['first_name'])) {
            $params ['first_name'] = $user['first_name'];
        } else {
            $fail = true;
        }
        if (isset($user['last_name'])) {
            $params ['last_name'] = $user['last_name'];
        } else {
            $fail = true;
        }
        if (isset($user['email'])) {
            $params ['email'] = $user['email'];
        } else {
            $fail = true;
        }
        if (isset($user['username'])) {
            $params ['username'] = $user['username'];
        } else {
            $fail = true;
        }
        if (isset($user['password'])) {
            $params ['password'] = md5($user['password']);
        } else {
            $fail = true;
        }
        if ($fail) {
            return array('code' => 5, 'message' => $message['user_missing_args'], 'class' => get_class($this), 'output' => '');
        }
        
        
        // Checks ADMIN option
        if ($admin) {
            $params['admin'] = 1;
        } else {
            $params['admin'] = 0;
        }
        


        // Add User
        $result = $this->usersTable->addUsers(array($params));
        if ($result['code'] != 1) {
            return $result;
        } else {
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }
    }
    
    
    
    /*
     * Updates an user
     * 
     * User can only change: first_name, last_name, email, password, username
     * 
     * Obs:. the admin status cannot be changed
     * 
     * $user = array(
     *              'userId' =>,        //id of user to update
     *              'firstName' =>,     //new first name
     *              'lastName' =>,      //new last name
     *              'email' =>,         //new email
     *              'password' =>       //new password - as real string
     *              'username' =>       //new username
     *              );
     * 
     * Values not set will retain their value
     */
    public function updateUser($user) {
        GLOBAL $message;

        
        // Check if required inputs were given
            $count = 0;
            if (!isset($user['userId'])){
                return array('code' => 2, 'message' => $message['user_missing_user_id'], 'class' => get_class($this), 'output' => '');
            }
        
        
        // If not changes are to be done return TRUE
            foreach ($user as $key => $value){
                if ($key == 'userId'){
                    continue;
                }
                $count++;
            }
            if ($count == 0) {
                return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
            }
        
            
        // Check if user exists and if so load it's info
            $usersLoad = $this->usersTable->getUsersBy(array('user_id' => $user['userId']));
            if (count($usersLoad['output']) == 0){
                return array('code' => 4, 'message' => $message['user_not_found'], 'class' => get_class($this), 'output' => '');
            } else {
                $userLoad = $usersLoad['output'][0];
            }
        
        // Check if user has permission
        $hasPermission = $this->hasPermission('user_edit', array('admin' => $userLoad['admin']));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }

        
        // Check if input is valid 
        //  -> Check if new email / username are not used by another user
        {
            $valid = true;
        }
        if (!$valid) {
            return array('code' => 2, 'message' => $message['user_invalid_input'], 'class' => get_class($this), 'output' => '');
        }

        
        // prepares array
        // 
        $params = array();
        if (isset($user['userId'])) {
            $params ['user_id'] = $user['userId'];
        }
        if (isset($user['firstName'])) {
            $params ['first_name'] = $user['firstName'];
        } 
        if (isset($user['lastName'])) {
            $params ['last_name'] = $user['lastName'];
        }
        if (isset($user['email'])) {
            $params ['email'] = $user['email'];
        }
        if (isset($user['password'])) {
            $params ['password'] = md5($user['password']);
        } 
        if (isset($user['username'])) {
            $params ['username'] =$user['username'];
        } 
                
        
      
        // Update User
        $result = $this->usersTable->updateUsers(array($params));
        if ($result['code'] != 1) {
            return $result;
        } else {
            
        // Update object
        if($params['user_id'] == $this->userInfo['user_id']){
                foreach ($params as $key => $value) {
                    $this->userInfo[$key] = $value;
                }
        }
                    
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }
    }
    
    
    
    /** @param int $userId
     *  
     *  removes an user
     * 
     *  @internal  will logout the user if he's logged in
     */
    public function deleteUser($userId) {
        GLOBAL $message;
        
        // Check if user exists and if so load it's info
            $usersLoad = $this->usersTable->getUsersBy(array('user_id' => $userId));
            if (count($usersLoad['output']) == 0){
                return array('code' => 4, 'message' => $message['user_not_found'], 'class' => get_class($this), 'output' => '');
            } else {
                $userLoad = $usersLoad['output'][0];
            }
        
            
        // Check if user has permission
        $hasPermission = $this->hasPermission('user_delete', array('admin' => $userLoad['admin']));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        
        // Error flag
        $fail = false;

        // Delete User Collections
        $userCollections = $this->collectionsTable->getCollectionsBy(array('owner_id' => $this->userInfo['user_id']));
        foreach ($userCollections['output'] as $userCollection) {
            $result = $this->deleteCollection($userCollection['collection_id']);
            if ($result['output'] != 1){
                $fail = true;
            }
        }

        // Delete User Groups
        $userGroups = $this->groupsTable->getGroupsBy(array('owner_id' => $this->userInfo['user_id']));
        foreach ($userGroups['output'] as $userGroup) {
            $result = $this->deleteGroup($userGroup['group_id']);
            if ($result['output'] != 1) {
                $fail = true;
            }
        }


        // If user is logged in log him out
        if ($this->loggedIn) {
            $this->logout();
        }


        // Delete user
        $result = $this->usersTable->deleteUsers(array($userId), 'user_id');
        if ($result['output'] != 1) {
            $fail = true;
        }


        if ($fail) {
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        } else {
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }
        
    }
    
    

    public function hasPermission($action, $options = null) {
        return true;
    }

    /**
     * Log in an user
     * 
     * @param username - string
     * @param password - real string password 
     * 
     * @return TRUE or FALSE
     */
    public function logIn($username, $password) {
        GLOBAL $message;


        //MD5 encode password
        $md5_pass = md5($password);

        //Check if there is already an user logged in
        if (Session::get_variable('loggedIn')) {
            return array('code' => 6, 'message' => $message['user_already_loggedIn'], 'class' => get_class($this), 'output' => '');
        }

        $users = $this->usersTable->getUsersBy(array('username' => $username));

        //Check if username is valid
        if (count($users['output']) == 0) {
            return array('code' => 7, 'message' => $message['user_incorrect_username'], 'class' => get_class($this), 'output' => '');
        }


        //Check if password is valid
        $user = $users['output'][0];
        if ($user['password'] == $md5_pass) {
            Session::set_variable('loggedIn', true);
            Session::set_variable('userId', $user['user_id']);

            $this->loggedIn = true;
            $this->userInfo = $user;

            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        } else {
            return array('code' => 8, 'message' => $message['user_incorrect_password'], 'class' => get_class($this), 'output' => '');
        }
    }

    
    /** 
     * Attempts to logout the user
     */
    public function logout() {
        
        // Check is there is an user logged in
        if (Session::get_variable('loggedIn')){
            Session::set_variable('loggedIn', false);
            Session::discard_variable('userId');
            
            $this->loggedIn = false;
            $this->userInfo = null;
        } else {
            return false;
        }
    }

    
    /** Returns TRUE or FALSE depending
     *  if this user is logged in 
     * 
     */
    public function isLoggedIn() {
        if (Session::get_variable('loggedIn')) {
            if (Session::get_variable('userId') == $this->userInfo['userId']) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    
    /*
     * Atemps to load the logged in user into the object
     * 
     * @ Returns TRUE or FALSE
     * 
     */
    public function loadLoggedUser() {
        if (Session::get_variable('loggedIn')) {

            // Checks if user exists
            $users = $this->usersTable->getUsersBy(array('user_id' => Session::get_variable('userId')));
            if ($users['code'] != 1) {
                return $users;
            } else {
                if (count($users['output']) == 0) {
                    return false;
                }

                // Loads userInfo
                $user = $users['output'][0];
                $this->userInfo = $user;
                $this->loggedIn = true;
                return true;
            }
        }
    }
    
    /*
     * Returns public info of this user
     * 
     * array ('user_id' =>, 'first_name' =>, 'last_name' =>, 'admin' =>)
     * 
     * Returns FALSE if the user is a guest
     * 
     */
    public function getUserPublicInfo () {
        GLOBAL $message;
        
        // Checks is user has permission
        $hasPermission = $this->hasPermission('user_info_access', array('privateInfo' => false, 'admin' => $this->userInfo['admin']));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        if ($this->userInfo == null){
            return array('code' => 9, 'message' => $message['user_object_not_loaded'], 'class' => get_class($this), 'output' => false);
        } else {
            $output = array();
            $output['userId'] = $this->userInfo['user_id'];
            $output['firstName'] = $this->userInfo['first_name'];
            $output['lastName'] = $this->userInfo['last_name'];
            $output['admin'] = $this->userInfo['admin'];
            
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => $output);
        }
    }

    
    /*
     * Returns all user info
     * array ('userId' =>, 'firstName' =>, 'lastName' =>, 'email' =>, 'username' =>, 'password' =>, 'admin' =>)
     * Returns FALSE if the user is not logged in
     */
    public function getUserInfo () {
        GLOBAL $message;
        
        // Checks is user has permission
        $hasPermission = $this->hasPermission('user_info_access', array('privateInfo' => true, 'admin' => $this->userInfo['admin']));
        if (!$hasPermission) {
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        if ($this->userInfo == null){
            return array('code' => 9, 'message' => $message['user_object_not_loaded'], 'class' => get_class($this), 'output' => false);
        } else {
            $output = array();
            $output['userId'] = $this->userInfo['user_id'];
            $output['firstName'] = $this->userInfo['first_name'];
            $output['lastName'] = $this->userInfo['last_name'];
            $output['email'] = $this->userInfo['email'];
            $output['username'] = $this->userInfo['username'];
            $output['password'] = $this->userInfo['password'];
            $output['admin'] = $this->userInfo['admin'];
            
            return array('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => $output);
        }
        
    }
    
    
    
    /*
     * Returns userId
     * 
     * Returns FALSE if the user is a guest / user object not loaded
     * 
     */
    public function getUserId () {
        GLOBAL $message;
        
        // Checks is user has permission
        $hasPermission = $this->hasPermission('user_info_access', array('privateInfo' => false, 'admin' => $this->userInfo['admin']));
        if (!$hasPermission) {
            return FALSE;
        }
        
        if ($this->userInfo == null){
            return FALSE;
        } else {
            return $this->userInfo['user_id'];
        }
    }

    
    
    /**
     * @param $collectionId intenger - id of collection
     * 
     * Returns TRUE or FALSE depending on whether is the owner
     * of the collection
     */
    public function isOwnerOf ($collectionId) {
        
        // Returns false if collection does not exist
        $collections = $this->collectionsTable->getCollectionsBy(array('collection_id' => $collectionId));
        if (count($collectionId['output']) == 0){
            return FALSE;
        } else {
            $collection = $collections['output'][0];
        }
        
        // Verifies if user is owner or not
        if ($this->userInfo['owner_id'] == $collection['owner_id']){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    
    /**
     * @param $collectionId intenger - id of collection
     * 
     * Returns TRUE or FALSE depending on whether user is admin
     * of collection 
     */
    public function isAdminOf ($collectionId){
    
             
}



}

?>
