<?php
include_once('Classes/Tableclasses/TagRelationshipsTable.php');

//
// CLASS TAGCLOUD
//
// output
// 
//  code 1 -> success
//  code 2 -> invalid input
//  code 3 -> no permission
//  code 4 -> tag not found

class Tagcloud {

    //info
    public $tagcloudId;
    
    //objects
    public $collection;
    public $user;
    
    //tables
    public $tagsTable;
    public $tagRelationshipsTable;
    
    //cache
    public $tags;
    public $tagRelationships;
    
    public function __construct($tagcloudId, $collection) {
        $this->tagcloudId = $tagcloudId;
        $this->collection = $collection;
        $this->user = $this->collection->user;
        
        $this->tagsTable = new TagsTable();
        $this->tagRelationshipsTable = new TagRelationshipsTable();
        
        $this->tags = $this->tagsTable->getTagsBy(array('tagcloud_id' => $this->tagcloudId));
        $this->tagRelationships = $this->tagRelationshipsTable->getAllRowsByTagcloudId($this->tagcloudId);
    }
    
    /*
     * Returns all tags of tagcloud as objects in an array
     */
    public function getAllTags () {
        $tags = array();
        
        foreach ($this->tags as $tag){
            $tags[] = new Tag($tag['tag_id'], $this);
        }
        
        return array ('code' => 1, 'message' => $message['tagcloud_success'], 'class' => get_class($this), 'output' => $tags);
    }
    
    
    /*
     * Returns an tag object based on the input id
     */
    public function getTagById($id){
        GLOBAL $message;
        
        // Check if tag exists
        $exists = false;
        foreach ($this->tags as $tag){
            if($tag['tag_id'] == $id){
                $exists = true;
                break;
            }
        }
        if (!$exists){
            return array ('code' => 4, 'message' => $message['tag_not_found'], 'class' => get_class($this), 'output' => '');
        }
        
        $tag = new Tag($id, $this);
        
        // Return tag object
            return array ('code' => 1, 'message' => $message['tagcloud_success'], 'class' => get_class($this), 'output' => $tag);
    }
    
    
    
    /*
     * Create tags
     * 
     * $tags = array($tag, ....)
     * 
     * $tag = array(
     *              'name' =>,
     *              'description' =>
     *              );
     */
    public function createTags($tags) {
        GLOBAL $message;
        
        
        $valid = true;
        // Check if input if valid
        {
        $valid = true;    
        }
        
        if(!$valid) {
            return array ('code' => 2, 'message' => $message['tagcloud_invalid_input'], 'class' => get_class($this), 'output' => '');
        }
        
        // Check if user has permission to access it
        $hasPermission = $this->user->hasPermission('collection_content_create_edit', array('collection_id' => $this->collectionId));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        $params = array();
        
        foreach ($tags as $tag) {
        $params[] = array(
                   'name' => $tag['tag_id'],
                   'description' => $tag['description'],
                   'tagcloud_id' => $this->tagcloudId,
                   );   
        }
        
                    
        $result = $this->tagsTable->addTags($params);
        if ($result['code'] != 1) {
            return $result;
        } else {
            return array ('code' => 1, 'message' => $message['collection_success'], 'class' => get_class($this), 'output' => '');
        }
    }
    
    
    /*
     *  $tagIds = array(id, ...)
     * 
     *  Removes tag by id
     */
    public function deleteTags ($tagIds) {
        GLOBAL $message;
        
        // Check if user has permission to access it
        $hasPermission = $this->user->hasPermission('collection_content_delete', array('collection_id' => $this->collectionId));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        
        // Check if images exists
        foreach ($tagIds as $tagId) {
            $fail = true;

            foreach ($this->tags as $tag) {

                if ($tag['tag_id'] == $tagId) {
                    $fail = false;
                    break;
                }
            }
        }
        if ($fail){
            return array ('code' => 4, 'message' => $message['tag_not_found'], 'class' => get_class($this), 'output' => '');
        }
        
        // Delete tags
        $result = $this->tagsTable->deleteTags($tagIds, 'tag_id');
        if ($result['code'] != 1){
            return $result;
        } else {
            return array ('code' => 1, 'message' => $message['tagcloud_success'], 'class' => get_class($this), 'output' => '');
        }
        
    }
    
}

?>
