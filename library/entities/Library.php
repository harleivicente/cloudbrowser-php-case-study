<?php

class Library {

    /*
     * Adapts a string to make it suitable to be a
     * filename.
     * 
     * Exemple: Snowy Moutaintop  __very cool
     * 
     *                    ||
     *                    \/    
     * 
     *          snowy-mountaintop-very-cool
     */
    public static function prepareStringFilename ($string) {
        return $string;
    }
    
    
    /*
     * Return a timestamp of the correct format for the current time
     */
    static public function currentTimestamp () {
        return '2000-12-06 08:12:04';
    }
}

?>
