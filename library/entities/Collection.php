<?php

include_once('Classes/TableClasses/CommentsTable.php');
include_once('Classes/TableClasses/ImageTagsTable.php');
include_once('Classes/Tagcloud.php');

//
//  CLASS COLLECTION
//  
//  Output
//  
//  code 1 -> success
//  code 2 -> invalid input
//  code 3 -> no permission
//  code 4 -> collection not found

class Collection {

    //info
    public $collectionId;
    private $collectionConfig;
    
    //objects
    public $user;
    public $tagcloud;
    
    //tables
    public $imagesTable;
    public $imageTagsTable;
    public $commentsTable;
    public $collectionsTable;
    
    //cache
    public $images;
    public $imageTags;
    public $comments;
    
    
    public function __construct($collectionId) {
        GLOBAL $message;
        
        $this->collectionId = $collectionId;
        $this->user = new User();
        
        $this->imagesTable = new ImagesTable();
        $this->commentsTable = new CommentsTable();
        $this->imageTagsTable = new ImageTagsTable();
        $this->collectionsTable = new CollectionsTable();
        
        $this->images = $this->imagesTable->getImagesBy(array('collection_id' => $this->collectionId));
        $this->imageTags = $this->imageTagsTable->getAllRowsByCollectionId($this->collectionId);
        $this->comments = $this->commentsTable->getAllCommentsByCollectionId($this->collectionId);
        $collections = $this->collectionsTable->getCollectionsBy(array('collection_id' => $this->collectionId));
        if (count($collections['output']) == 0) {
            die(print_r(array('code' => 4, 'message' => $message['collection_not_found'], 'class' => get_class($this), 'output' => '')));
        } else {
            $this->collectionConfig = $collections['output'][0];
        }
      
        
        $this->tagcloud = new Tagcloud($this->collectionConfig['tagcloud_id'], $this);
    }
    
    
    /*
     * Returns all images of collection as objects
     */
    public function getAllImages() {
        GLOBAL $message;
        
        $imageIds = array();
        foreach ($this->images as $image){
            $imageIds[] = $image['image_id'];
        }
        
        //getting image objets
        $images = array();
        foreach ($imageIds as $imageId){
            $images[] = new Image($imageId, $this);
        }
        
        return array ('code' => 1, 'message' => $message['collection_success'], 'class' => get_class($this), 'output' => $images);

    }
    
    
    /*
     * $tags = array (tagId_1, tagId_2)
     * 
     * Return an array of image objects that have one or more of the tags
     * 
     */
    public function getImagesByTags ($tagIds) {
        GLOBAL $message;
        
        $imageIds = array();
        $imagesCheckedIds = array();
        
        foreach ($imageIds as $imageId) {
            
            if (array_search($imageId, $imagesCheckedIds) === false) {
                $imagesCheckedIds[] = $imageId;
            } else {
                continue;
            } 
                
            
            foreach ($tagIds as $tagId) {
                
                if ($image['tag_id'] == $tagId){
                    $imageIds[] = $imageId;
                    break;
                }
                
            }
        }
        
        
        //getting image objects
        $images = array();
        
        foreach ($imageIds as $imageId) {
        $images[] = new Image($imageId, $this);    
        }
        
        return array ('code' => 1, 'message' => $message['collection_success'], 'class' => get_class($this), 'output' => $images);
    }
    
    
    /* $images = array ($info, ...)
     * 
     * $info = array(
     *              'title' =>,
     *              'description' =>
     *              );
     */
    public function createImages ($images) {
        GLOBAL $message;
        
        
        $valid = true;
        // Check if input if valid
        {
        $valid = true;    
        }
        
        if(!$valid) {
            return array ('code' => 2, 'message' => $message['collection_invalid_input'], 'class' => get_class($this), 'output' => '');
        }
        
        // Check if user has permission to access it
        $hasPermission = $this->user->hasPermission('collection_content_create_edit', array('collection_id' => $this->collectionId));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        $params = array();
        
        foreach ($images as $image) {
        $params[] = array(
                   'collection_id' => $this->collectionId,
                   'title' => $image['title'],
                   'description' => $image['description'],
                   'views' => 0,
                   'average_rating' => null,
                   'date_created' => Library::currentTimestamp(),
                   'last_edit_on' => Library::currentTimestamp(),
                   'last_edit_by' => $this->user->userId,
                   'path' => "$this->collectionId" . '_' . Library::prepareStringFilename($info['title'])
                    );    
        }
        
                    
        $result = $this->imagesTable->addImages($params);
        if ($result['code'] != 1) {
            return $result;
        } else {
            return array ('code' => 1, 'message' => $message['collection_success'], 'class' => get_class($this), 'output' => '');
        }
    }
    
    
    /*
     *  $imageIds = array(id, ...)
     * 
     *  Removes images by id
     */
    public function deleteImages ($imageIds) {
        GLOBAL $message;
        
        // Check if user has permission to access it
        $hasPermission = $this->user->hasPermission('collection_content_delete', array('collection_id' => $this->collectionId));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        
        // Check if images exists
        foreach ($imageIds as $imageId) {
            $fail = true;

            foreach ($this->images as $image) {

                if ($image['image_id'] == $imageId) {
                    $fail = false;
                    break;
                }
            }
        }
        if ($fail){
            return array ('code' => 4, 'message' => $message['image_not_found'], 'class' => get_class($this), 'output' => '');
        }
        
        // Delete images
        $result = $this->imagesTable->deleteImages($imageIds, 'image_id');
        if ($result['code'] != 1){
            return $result;
        } else {
            return array ('code' => 1, 'message' => $message['collection_success'], 'class' => get_class($this), 'output' => '');
        }
        
    }
    
    
     /*
     * Configs a collection
     * 
     * $collection  =  array(
     *                      'collection_id' =>,
     *                      'title' =>,
     *                      'short_description' => ,
     *                      'full_description' =>,
     *                      'category_id' =>,
     *                      'access_type' =>,
     *                      'access_mode' =>,
     *                      );
     */
        public function configCollection ($collection) {
        GLOBAL $message;

        // Check if user has permission to edit it
        $hasPermission = $this->user->hasPermission('collection_edit', array('collection_id' => $this->collectionId));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }

        // Check if input is valid
        $valid = true;
        {
        $valid = true;
        }
        if (!$valid) {
            return array ('code' => 2, 'message' => $message['user_invalid_input'], 'class' => get_class($this), 'output' => '');
        }


            $params = array(
                            'collection_id' => $this->collectionId,
                            'last_edit_on' => Library::currentTimestamp(),
                            'last_edit_by' => $this->user->getUserId(),
                            );
            
            if (isset($collection['title'])) {
                $params['title'] = $collection['title'];
            }
            if (isset($collection['short_description'])) {
                $params['short_description'] = $collection['short_description'];
            }
            if (isset($collection['full_description'])) {
                $params['full_description'] = $collection['full_description'];
            }
            if (isset($collection['category_id'])) {
                $params['category_id'] = $collection['category_id'];
            }
            if (isset($collection['access_type'])) {
                $params['access_type'] = $collection['access_type'];
            }
            if (isset($collection['access_mode'])) {
                $params['access_mode'] = $collection['access_mode'];
            }
                            
                
            
        // Update collection
        $result = $this->collectionsTable->updateCollections(array($params));
        if ($result['code'] != 1){
            return $result;
        } else {
            return array ('code' => 1, 'message' => $message['user_success'], 'class' => get_class($this), 'output' => '');
        }

        }
        
        
        
        public function getConfig () {
            GLOBAL $message;
            
            //Check if user has permission
        $hasPermission = $this->user->hasPermission('collection_configuration_acess', array('privateCollection' => $this->collectionConfig['access_type']));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
            return array ('code' => 1, 'message' => $message['collection_success'], 'class' => get_class($this), 'output' => $this->collectionConfig);
        }
        
        /**
         *
         * @param array $entry - array with the format :
         * array (
         *      entry_type =>, 
         *      user_group_id =>
         * )
         * entry type 0 - user id
         * entry type 1 - group id
         *      
         * @return TRUE or FALSE
         */
        public function addAdminEntry ($entry) {
            
        //Check if user has permission
        $hasPermission = $this->user->hasPermission('collection_edit', array('collectionId' => $this->collectionId));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }    
        
        //Check if input is consistent

        
        }
    
}

?>
