<?php

class Session {

 /*
 * Starts an DATA SESSION
 */
static private function startDataSession() {
        if (!self::isDataSessionStarted()) {
            self::initializeSessionVar();
            $_SESSION['session_started'] = true;
        }
    }    

/*
 * Checks if DATA SESSION has been started returning TRUE | FALSE
 */
static private function isDataSessionStarted(){

    self::initializeSessionVar();
    
    if (isset($_SESSION['session_started'])){
        if ($_SESSION['session_started'] == true){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/*
 * Ends an DATA SESSION
 */
static private function endDataSession() {
    if (self::isDataSessionStarted()){
        self::initializeSessionVar();
        $_SESSION['session_started'] = false;
    }
}

/*
 * Sets the value of a DATA SESSION variable
 * 
 * @ INVALID VARIABLE 'session_started'
 */
static public function set_variable($var_name, $value) {
    if(!self::isDataSessionStarted()){
        self::startDataSession();
    }
    self::initializeSessionVar();
    
    if ($var_name != 'session_started'){
        $_SESSION[$var_name] = $value;
    }
}


/*
 * Gets one or all DATA SESSION variables (if no variable name is given)
 * 
 * @ Returns FALSE if variable does not exist
 * @ Return value of variable
 * @ Return array of values of variables
 * 
 */
static public function get_variable($var_name = null) {
    if(!self::isDataSessionStarted()){
        return false;
    }
    self::initializeSessionVar();
    
    if ($var_name == null){
        $values = array();
        foreach ($_SESSION as $variable => $value){
            if ($variable == 'session_started'){
                continue;
            }
            $values[$variable] = $value;
        }
        return $values;
    } else {
    if(!isset($_SESSION[$var_name])){
        return false;
    } else {
        return $_SESSION[$var_name];
    }    
    }
}

/*
 * Discards one or all DATA SESSION variables (if no variable name is given)
 * 
 */
static public function discard_variable($var_name = null) {
    if (self::isDataSessionStarted()){
        self::initializeSessionVar();
        
        if ($var_name == null){
            foreach ($_SESSION as $key => $value){
                if ($key == 'session_started'){
                    continue;
                }
            unset ($_SESSION[$key]);
            }
        } else {
            
            if (isset($_SESSION[$var_name])&&($var_name != 'session_started')){
                unset($_SESSION[$var_name]);
            }
        }
    
    }

}


/*
 * Initializes $_SESSION variable if needed
 */
static private function initializeSessionVar() {
    if (!isset($_SESSION)){
        session_start();
    }
}

    
}

?>
