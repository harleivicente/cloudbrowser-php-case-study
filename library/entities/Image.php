<?php
require_once('Configurations/configurations.php');
require_once('Classes/Security.php');
require_once('Classes/Session.php');
require_once('Classes/Collection.php');

    /* CLASS IMAGE
     * 
     * return type
     * 
     * code => 1 success
     * code => 2 invalid data input
     * code => 3 no permission
     * 
     */
    
class Image {
    
    //info
    public $imageId;
    
    //objects
    public $collection;
    public $user;
    
    public function __construct($imageId, $collection) {
        $this->imageId = $imageId;
        $this->collection = $collection;
        
        $this->user = $this->collection->user;
    }
    
    
    //Returns an array of objects Tags to which an image is directly
    //related
    public function getAllTags () {
        GLOBAL $message;
        
        $imageTags = $this->collection->imageTags;
        
        $tagIds = array();
        foreach ($imageTags as $relationship){
            if ($relationship['image_id'] == $this->imageId) {
                $tagIds[] = $relationship['tag_id'];
            }
        }
        $tags = array();
        foreach ($tagIds as $id){
            $tags[] = new Tag($id, $this->collection->tagcloud);
        }
        
        return array('code' => 1, 'message' => $message['image_success'], 'class' => get_class($this), 'output' => $tags);
    }
    
    
    
    // Get all info from Images table for this image
    //
    public function getInfo(){
        GLOBAL $message;
       $images = $this->collection->images;
       
       // Check if user has permission
        $hasPermission = Security::hasActionPermission('collection_content_access', array('collection_id' => $this->collection->collectionId));
        if (!$hasPermission){
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
       
       
       foreach ($images as $image){
           if ($image['image_id'] == $this->imageId){
               $output = $image;
               break;
           }
       }
        
        return array('code' => 1, 'message' => $message['image_success'], 'class' => get_class($this), 'output' => $output);
        
    }
    
    
    
    /* Update image info
     * 
     * $info = array (
     *          'title' =>,
     *          'description' =>,
     *          'path' =>
     * );
     * 
     * Values not set will retain their current value
     * 
     */
    public function updateImageInfo ($info) {
        GLOBAL $message;
        
        // Check if user has permission
        $hasPermission = $this->user->hasPermission('collection_content_create_edit', array('collection_id' => $this->collection->collectionId));
        if (!$hasPermission){
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }

        
        // Control content to be input
        {
            $fail = false;            
        }
        if($fail) {
            return array('code' => 2, 'message' => $message['image_invalid_input'], 'class' => get_class($this), 'output' => '');
        }
        
        
        // Place date in appropriate array
        $input = array();
        if (isset($info['title'])) { $input['title'] = $info['title']; }
        if (isset($info['description'])) { $input['description'] = $info['description']; }
        if (isset($info['path'])) { $input['path'] = $info['path']; }
                
        $output = $this->collection->imagesTable->updateImages( array($input) );
        
        if ($output['code'] == 1){
            return array('code' => 1, 'message' => $message['image_info_update_success'], 'class' => get_class($this), 'output' => '');
        } else {
            return $output;
        }
        
    }
    
    
    /*
     * $comment = string to set as comment
     * 
     * 
     */
    public function commentImage ($comment) {
    GLOBAL $message;
    
    $commentsTable = $this->collection->commentsTable;    
    
    // Check if user has permission
    $hasPermission = $this->user->hasPermission('collection_comment', array('collection_id' => $this->collection->collection_id));
    if (!$hasPermission){
            return array('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
    
    // Control content to be input
    {
        $fail = false;            
    }
    if($fail) {
        return array('code' => 2, 'message' => $message['image_invalid_input'], 'class' => get_class($this), 'output' => '');
    }
    
    
    // Adding comment
    
    $comment =  array ('user_id' => Session::getLoggedInUserId(), 'image_id' => $this->imageId, 'comment' => $comment);
    
    $output = $commentsTable->addComments(array($comment));
    if ($output['code'] == 1){
            return array('code' => 1, 'message' => $message['image_comment_add_success'], 'class' => get_class($this), 'output' => '');
        } else {
            return $output;
        }
    
    }
    
}


?>
