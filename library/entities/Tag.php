<?php
require_once('Configurations/configurations.php');
require_once('Classes/Security.php');
require_once('Classes/Session.php');
require_once('Classes/Collection.php');
require_once('Classes/Tagcloud.php');



/* CLASS TAG
    * 
    * return type
    * 
    * code => 1 success
    * code => 3 no permission
    * 
    */

class Tag {
    
    //info
    public $tagId;
    
    //object
    public $tagcloud;
    public $user;
    
    
    public function _construct ($tagId, $tagcloud) {
        $this->tagId = $tagId;
        $this->tagcloud = $tagcloud;
        $this->user = $this->tagcloud->collection->user;
    }
    
    
        
     /*
     * Returns the information avaiable for a tag
     */
    public function getInfo () {
        GLOBAL $message;
        
        // Check if user has permission to access it
        $hasPermission = $this->user->hasPermission('collection_content_access', array('collection_id' => $this->tagcloud->collection->collectionId));
        if (!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        $tags = $this->tagcloud->tags;
        foreach ($tags as $tag) {
            if ($tag['tag_id'] == $this->tagId){
                $output = $tag;
            }
        }
           return array ('code' => 1, 'message' => $message['tag_success'], 'class' => get_class($this), 'output' => $output);
        
    }
    
    
    /*
     * Update information for tag
     * 
     * $info = array(
     *       'name' =>,
     *       'description'
     * );
     *
     * Values not set will retain their value
     *  
     */
    public function updateTagInfo($info) {
        GLOBAL $message;
        
        //Check if user has permission
        $hasPermission = $this->user->hasPermission('collection_content_create_edit', array('collection_id' => $this->tagcloud->collection->collectionId));
        if(!$hasPermission){
            return array ('code' => 3, 'message' => $message['no_permission'], 'class' => get_class($this), 'output' => '');
        }
        
        //Check if content is acceptable
        {
            $fail =  false;
        }
        if($fail){
            return array('code' => 2, 'message' => $message['tag_invalid_input'], 'class' => get_class($this), 'output' => '');
        }
        
        
        //Update info
        $tag = array('name' => $info['name'], 'description' => $info['description']);
        $output = $this->tagcloud->tagsTable->updateTags(array($tag));
        if ($output['code'] == 1) {
            return array('code' => 1, 'message' => $message['tag_success'], 'class' => get_class($this), 'output' => '');
        } else {
            return $output;
        }
    }
    
    
    /*
     * Returns as array of objects Tags that are children of this tag
     */
    public function getChildren() {
        GLOBAL $message;
        
        $tagRelationships = $this->tagcloud->tagRelationships;
        
        $childrenIds = array();
        
        foreach ($tagRelationships as $tagRelationship){
            if ($tagRelationship['parent_tag'] == $this->tagId){
                $childrenIds[] = $tagRelationship['child_tag'];
            }
        }
        
        
        //Creating objects of children
        $children = array();
        
        foreach ($childrenIds as $childId){
            $children[] = new Tag($childId, $this->tagcloud);
        }
        
        return array ('code' => 1, 'message' => $message['tag_success'], 'class' => get_class($this), 'output' => $children);
        
    }
    
    
     /*
     * Returns as array of objects Tags that are a parent of this tag
     */
    public function getParents() {
        GLOBAL $message;
        
        $tagRelationships = $this->tagcloud->tagRelationships;
        
        $parentIds = array();
        
        foreach ($tagRelationships as $tagRelationship){
            if ($tagRelationship['child_tag'] == $this->tagId){
                $parentIds[] = $tagRelationship['parent_tag'];
            }
        }
        
        
        //Creating objects of parents
        $parents = array();
        
        foreach ($parentIds as $parentId){
            $parents[] = new Tag($parentId, $this->tagcloud);
        }
        
        return array ('code' => 1, 'message' => $message['tag_success'], 'class' => get_class($this), 'output' => $parents);
        
    }
    
    
    /*
     * return an array of objetcs Tag that are ancestors of this tag
     * IF no ancestors exists, RETURNS EMPTY ARRAY
     * 
     */
    public function getAncestors() {
        GLOBAL $message;
        
        $ancestors = array();
        $stack = array();

        $parents = $this->getParents($this->tagId);

        $ancestors = array_merge($ancestors, $parents);
        $stack = array_merge($stack, $parents);

        while (true) {

            if (count($stack) == 0) {
                break;
            }

            $first = $stack[0];

            if (array_search($first, $ancestors) === false) {
                $ancestors[] = $first;
            }

            $parents = $this->getParents($first);
            $stack = array_merge($stack, $parents);


            array_shift($stack);
        }
        
        return array ('code' => 1, 'message' => $message['tag_success'], 'class' => get_class($this), 'output' => $ancestors);
    }
    

    
    /*
     * return an array of objetcs Tag that are descendants of this tag
     * IF no descendants exists, RETURNS EMPTY ARRAY
     * 
     */
    public function getDescendants() {
        GLOBAL $message;
        
        $descendants = array();
        $stack = array();

        $children = $this->getChildren($this->tagId);

        $descendants = array_merge($descendants, $children);
        $stack = array_merge($stack, $children);

        while (true) {

            if (count($stack) == 0) {
                break;
            }

            $first = $stack[0];

            if (array_search($first, $descendants) === false) {
                $descendants[] = $first;
            }

            $children = $this->getChildren($first);
            $stack = array_merge($stack, $children);


            array_shift($stack);
        }
        
        return array ('code' => 1, 'message' => $message['tag_success'], 'class' => get_class($this), 'output' => $descendants);
    }
}
?>
