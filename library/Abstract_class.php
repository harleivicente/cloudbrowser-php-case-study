<?php 

class Abstract_class {
	
	public static function __callStatic($name, $arguments){
		$class = get_called_class();
		echo "The method: $name was not implemented. Method sought in the class: $class";
	}

	public function __call($name, $arguments){
		$class = get_called_class();	
		echo "The method: $name was not implemented. Method sought in the class: $class";
	}

}

?>