<?php

class Viewer extends Abstract_class{

    /*
     *   render_frame - Generates the string for the frame. Frames are withing
     *   the site/view/frame or app/<app_name>/site/view/frame folder.
     */
    public function render_frame($frame_name, $params) {


        // Run before anything in case js or css is loaded within the frame file.
        $frame = Viewer::render(Utilities::get_frame_path($frame_name) . "/" . $frame_name . ".php", $params);        
        
        $js_files = Utilities::extract_config(array('loaded_elements', 'js'));
        if(is_array($js_files)){
            $js = "";
            foreach($js_files as $js_file){
                $js .= Viewer::render_html('source_js', array('src' => '/cloudbrowser/' . Utilities::get_js_path($js_file). "/" . $js_file . ".js"));
            }
        }

        $css_files = Utilities::extract_config(array('loaded_elements', 'css'));
        if(is_array($css_files)){
            $css = "";
            foreach($css_files as $css_file){
                $css .= Viewer::render_html('link_css', array('href' => '/cloudbrowser/' . Utilities::get_css_path($css_file). "/" . $css_file . ".css"));
            }
        }

        // Generating message html.
        $messages_array = Utilities::extract_system_messages();
        $messages = Viewer::render_component('system_messages', array('messages' => $messages_array));

        $html = "<html> <head> $js $css </head>";
        $html .= "<body> $messages $frame </body> </html>";
        
        
        return $html;
    }

    /*
     *   render_object - Generates the string for the object. Objects are withing
     *   the site/view/object or app/<app_name>/site/view/object folder.
     */
    public function render_object($object_name, $params) {
        return Viewer::render(Utilities::get_object_path($object_name) . "/" . $object_name . ".php", $params);
    }

    /*
     *   render_page - Generates the string for the page. Pages are withing
     *   the site/view/page or app/<app_name>/site/view/page folder.
     */
    public function render_page($page_name, $params) {
        return Viewer::render(Utilities::get_page_path($page_name) . "/" . $page_name . ".php", $params);
    }

    /*
     *   render_component - Generates the string for the component. Components are withing
     *   the site/view/component or app/<app_name>/site/view/component folder.
     */
    public function render_component($component_name, $params) {
        return Viewer::render(Utilities::get_component_path($component_name) . "/" . $component_name . ".php", $params);
    }

    /*
     *   render_html - Generates the string for the html element. Html elements are withing
     *   the site/view/html or app/<app_name>/site/view/html folder.
     */
    public function render_html($html_name, $params) {
        return Viewer::render(Utilities::get_html_path($html_name) . "/" . $html_name . ".php", $params);
    }



    /**
     * load_js - Will register a .js file to be included when the element is viewed.
     * 
     * @param string $js - Name of .js file (without .js).
     *
     */
    public function load_js($js) {

        // Verifies if .js file exists
        $path = Utilities::get_js_path($js);

        if($path === false){
            throw new Exception("The js file: $js, was not found.");
        } else {
            if(!file_exists($path . "/" . $js . '.js')){
               throw new Exception("The js file: $js, was not found."); 
           } else {

            // Register the js in the $config enviroment variable.
            Utilities::push_config(array('loaded_elements', 'js'), $js);
        }
    }
}


    /**
     * load_css - Registers a .css file to be included when the element is viewed.
     * 
     * @param string $css - Name of css file (without .css).
     * 
     */
    public function load_css($css) {

    // Verifies if .css file exists
        $path = Utilities::get_css_path($css);

        if($path === false){
            throw new Exception("The css file: $css, was not found.");
        } else {
            if(!file_exists($path . "/" . $css . '.css')){
               throw new Exception("The css file: $css, was not found."); 
           } else {

            // Register the css in the $config enviroment variable.
            Utilities::push_config(array('loaded_elements', 'css'), $css);
        }
    }
}


private function render($file, $params) {
    GLOBAL $config;
    ob_start(); 
    if(!file_exists($file)){
        throw new Exception("The file $file, was not found.");
    } else {
        require($file);
        $output = ob_get_clean();
        return $output;
    }
}
}

?>
