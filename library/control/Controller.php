<?php

class Controller extends Abstract_class{

    public function __construct(){
    }
    

    /**
     * view_frame - Renders a frame.
     * 
     * @param string $frame_name - name of frame on folder site/view/frame to render.
     * @param mixed $params - variables to pass to the frame view.
     *
     */
    public function view_frame ($frame_name, $params) {

        $viewer = new Viewer();

        // verifies if frame exists
        if(!isset($frame_name)){
            throw new Exception('Frame name was not defined.');
        } else {
            $path = Utilities::get_frame_path($frame_name); 
            if(!$path || !file_exists($path . "/$frame_name.php")){
                throw new Exception("Frame was not found: $frame_name.");
            } else {
                echo $viewer->render_frame($frame_name, $params);
            }
        }
    }

}

?>
