<?php 
/*
* @uses $params['messages'].
*/
$messages = $params['messages'];
?>

<div id='system_messages'>

<?php 
foreach($messages as $type => $messages_type){
	echo "<div class ='system_messages_type'> $type </div>";
	foreach ($messages_type as $message_type){
		echo Viewer::render_object('system_message', array('message' => $message_type));
	}
}
?>

 </div>


