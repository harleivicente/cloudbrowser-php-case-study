<?php

$english = array(
        //Permissions
        'no_permission' => 'You do not have permission to perform this action',
    
        //Images
        'image_not_found' => 'Image does not exist or was not found',
    
        //Users
        'user_not_found' => 'User does not exist or was not found',
    
        //Tags
        'tag_not_found' => 'Tag does not exist or was not found',
    
        //Collections
        'collection_not_found' => 'Collection does not exist or was not found',
    
        //Groups
        'group_not_found' => 'Group does not exist or was not found',
        //-------------------------------------------------------------------//
        //>>>>>>>>>>>>>>>>>>>>>>>   CLASSES   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
    
        //Table Class
        'db_success' => 'Database query executed successfully',
        'db_unknown' => 'Unknown database error',
        'db_inconsistent' => 'Inconsistent data given',
    
        //Image Class
        'image_success' => 'Function from Image class executed successfully',
        'image_invalid_input' => 'Function from Image class was given invalid input',
        'image_info_update_sucess' => 'Image information updated successfully',
        'image_comment_add_success' => 'Comment added to image successfully',
    
        //Tag Class
        'tag_success' => 'Function from Tag class executed successfully',
        'tag_invalid_input' => 'Function from Tag class was given invalid input',
    
        //Collection Class
        'collection_success' => 'Function from Collection class executed suffessfully',
        'collection_invalid_input' => 'Function from Collection class was given invalid input',
    
        //Tagcloud Class
        'tagcloud_success' => 'Function from Tagcloud class executed successfully',
        'tagcloud_invalid_input' => 'Function from tagcloud class was given invalid input',
    
        //User Class
        'user_success' => 'Function from user class executed successfully',
        'user_invalid_input' => 'Function from user class was given invalid input',
        'user_missing_args' => 'Function from user class did not receive all the args',
        'user_already_loggedIn' => 'An user is already logged in',
        'user_incorrect_username' => 'Incorrect username given',
        'user_incorrect_password' => 'Incorrect password given',
        'user_object_not_loaded' => 'User object not loaded',
        'user_missing_user_id' => 'User function did not receive an userId',
        'user_delete_fail' => 'Error occurred while deleting the user'
);

?>
