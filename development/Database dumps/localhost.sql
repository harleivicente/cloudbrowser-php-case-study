-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2012 at 02:51 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cloudbrowser`
--
DROP DATABASE `cloudbrowser`;
CREATE DATABASE `cloudbrowser` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cloudbrowser`;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `collection_admins`
--

CREATE TABLE IF NOT EXISTS `collection_admins` (
  `collection_admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  PRIMARY KEY (`collection_admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE IF NOT EXISTS `collections` (
  `collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `short_description` text NOT NULL,
  `full_description` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `cover_path` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `tagcloud_id` int(11) NOT NULL,
  `access_type` int(11) NOT NULL,
  `access_mode` int(11) NOT NULL,
  `number_of_visits` int(11) NOT NULL,
  `number_of_images` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_edit_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_edit_by` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  PRIMARY KEY (`collection_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`collection_id`, `title`, `short_description`, `full_description`, `owner_id`, `cover_path`, `category_id`, `tagcloud_id`, `access_type`, `access_mode`, `number_of_visits`, `number_of_images`, `date_added`, `last_edit_on`, `last_edit_by`, `rating`) VALUES
(118, 'Harleis collection', 'Real short description', 'Real long description', 67, '118_cover', 2012, 118, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(119, 'Harleis collection', 'Real short description', 'Real long description', 67, '119_cover', 2012, 119, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 20),
(120, 'Harleis collection', 'Real short description', 'Real long description', 67, '120_cover', 2012, 120, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(121, 'Harleis collection', 'Real short description', 'Real long description', 67, '121_cover', 2012, 121, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(122, 'Harleis collection', 'Real short description', 'Real long description', 67, '122_cover', 2012, 122, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(123, 'Harleis collection', 'Real short description', 'Real long description', 67, '123_cover', 2012, 123, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(124, 'Harleis collection', 'Real short description', 'Real long description', 67, '124_cover', 2012, 124, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(125, 'Harleis collection', 'Real short description', 'Real long description', 67, '125_cover', 2012, 125, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(126, 'Harleis collection', 'Real short description', 'Real long description', 67, '126_cover', 2012, 126, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(127, 'Harleis collection', 'Real short description', 'Real long description', 67, '127_cover', 2012, 127, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(128, 'Harleis collection', 'Real short description', 'Real long description', 67, '128_cover', 2012, 128, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0),
(129, 'Harleis collection', 'Real short description', 'Real long description', 67, '129_cover', 2012, 129, 4, 4, 0, 0, '2000-12-06 10:12:04', '2000-12-06 10:12:04', 67, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `user_id`, `image_id`, `comment`) VALUES
(1, 1232, 12323, 'rather cool :D...blah and this and that.....');

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE IF NOT EXISTS `entries` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE IF NOT EXISTS `group_members` (
  `group_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`group_member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `image_tags`
--

CREATE TABLE IF NOT EXISTS `image_tags` (
  `image_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`image_tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `image_tags`
--

INSERT INTO `image_tags` (`image_tag_id`, `image_id`, `tag_id`) VALUES
(1, 22, 12),
(2, 32, 12),
(3, 22, 14),
(21, 32, 122),
(22, 32313, 1232);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `average_rating` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_edit_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_edit_by` int(11) NOT NULL,
  `path` text NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=323140 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`image_id`, `collection_id`, `title`, `description`, `views`, `average_rating`, `date_created`, `last_edit_on`, `last_edit_by`, `path`) VALUES
(22, 92, 'yes', 'blah', 0, 89, '2012-09-23 22:51:03', '0000-00-00 00:00:00', 123, ''),
(32, 92, 'no', 'blah 2', 0, 12, '2012-09-23 22:51:34', '0000-00-00 00:00:00', 12343, 'erer'),
(323138, 56, 'adf', 'adfadfdf', 0, 34, '2012-09-23 23:54:25', '0000-00-00 00:00:00', 232232, 'adfdfadfafdafd'),
(323139, 3, 'adf', '3', 0, 3, '2012-10-09 01:26:32', '0000-00-00 00:00:00', 3, '23');

-- --------------------------------------------------------

--
-- Table structure for table `tag_relationships`
--

CREATE TABLE IF NOT EXISTS `tag_relationships` (
  `relationship_id` int(11) NOT NULL AUTO_INCREMENT,
  `child_tag` int(11) NOT NULL,
  `parent_tag` int(11) NOT NULL,
  PRIMARY KEY (`relationship_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `tag_relationships`
--

INSERT INTO `tag_relationships` (`relationship_id`, `child_tag`, `parent_tag`) VALUES
(21, 2, 1),
(22, 3, 1),
(23, 5, 3),
(24, 5, 4),
(25, 102, 104),
(26, 103, 104),
(27, 103, 105);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `tagcloud_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `name`, `description`, `tagcloud_id`) VALUES
(12, 'some tag', 'blah', 82),
(14, 'random text', 'blah 23454', 82);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
