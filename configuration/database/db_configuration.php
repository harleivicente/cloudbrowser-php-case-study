<?php

$collections = array(
    'collection_id' =>
    array('type' => 'integer', 'default_value' => ''),
    'title' =>
    array('type' => 'string', 'default_value' => 'Dont have a title.s'),
    'short_description' =>
    array('type' => 'string', 'default_value' => 'No description set'),
    'full_description' =>
    array('type' => 'string', 'default_value' => 'No description set'),
    'owner_id' =>
    array('type' => 'integer', 'default_value' => 0),
    'cover_path' =>
    array('type' => 'string', 'default_value' => 'No path defined'),
    'category_id' =>
    array('type' => 'integer', 'default_value' => 0),
    'tagcloud_id' =>
    array('type' => 'integer', 'default_value' => 0),
    'access_type' =>
    array('type' => 'integer', 'default_value' => 0),
    'access_mode' =>
    array('type' => 'integer', 'default_value' => 0),
    'number_of_visits' =>
    array('type' => 'integer', 'default_value' => 0),
    'number_of_images' =>
    array('type' => 'integer', 'default_value' => 0),
    'date_added' =>
    array('type' => 'timestamp', 'default_value' => '2000-10-1 22:33:33'),
    'last_edit_on' =>
    array('type' => 'timestamp', 'default_value' => '2000-10-1 10:10:10'),
    'last_edit_by' =>
    array('type' => 'integer', 'default_value' => 0),
    'rating' =>
    array('type' => 'integer', 'default_value' => 0)
);

$images = array(
    'image_id' =>
    array ('type' => 'integer', 'default_value' => NULL),
    'collection_id' =>
    array ('type' => 'integer', 'default_value' => 0),
    'title' =>
    array('type' => 'string', 'default_value' => 'No title set'),
    'description' =>
    array('type' => 'string', 'default_value' => 'No description set'),
    'views' =>
    array('type' => 'integer', 'default_value' => 0),
    'average_rating' =>
    array('type' => 'integer', 'default_value' => NULL),
    'date_created' =>
    array('type' => 'timestamp', 'default_value' => '2000-10-1 10:10:10'),
    'last_edit_on' =>
    array('type' => 'timestamp', 'default_value' => '2000-10-1 10:10:10'),
    'last_edit_by' =>
    array('type' => 'integer', 'default_value' => 0),
    'path' =>
    array('type' => 'string', 'default_value' => 'No path set')
);

$tags = array(
    'tag_id' =>
    array('type' => 'integer', 'default_value' => 0),
    'name' =>
    array('type' => 'string', 'default_value' => 'No name was set'),
    'description' =>
    array('type' => 'string', 'default_value' => 'No description was set'),
    'tagcloud_id' =>
    array('type' => 'integer', 'default_value' => 0)
);

$users = array (
    'user_id' =>
    array('type' => 'integer', 'default_value' => ''),
    'first_name' =>
    array('type' => 'string', 'default_value' => 'not set'),
    'last_name' =>
    array('type' => 'string', 'default_value' => 'not set'),
    'email' =>
    array('type' => 'string', 'default_value' => 'not set'),
    'password' =>
    array('type' => 'string', 'default_value' => 'not set'),
    'username' =>
    array('type' => 'string', 'default_value' => 'not set'),
    'admin' =>
    array('type' => 'intenger', 'default_value' => 0)
);

$groups = array (
    'group_id' =>
    array('type' => 'integer', 'default_value' => ''),
    'owner_id' =>
    array('type' => 'integer', 'default_value' => 0),
    'name' =>
    array('type' => 'string', 'default_value' => 'not set')
);

$db_table_config = array (
                         'collections' => $collections,
                         'images' => $images,
                         'tags' => $tags,
                         'users' => $users,
                         'groups' => $groups
                         );












?>
